
## Installation

### Install Dependencies
 `npm install`

### Develop
 `npm run dev`

### Prod
1. `npm run build`
2. `npm run start`

### Change Port
  `PORT=XXXX ...`
  etc: `PORT=3002 npm run dev`