import App from '../components/App'
import Header from '../components/smart/Header'
import Footer from '../components/smart/Footer'
import VideoSingle from '../components/smart/Posts/VideoSingle'
import React from 'react'
import api from '../utils/api'
import ProgressPage from "../components/ProgressPage";

class VideoPage extends React.Component {

    static async getInitialProps({query, res, req}) {
        let result = [];
        let relatedVideos = [];

        if (query.videoId){
            result = await api.get("videos/" + query.videoId + '?expand=like',{}, req);
            if(result.statusCode == 402 && res){
                res.redirect('/');
                return{};
            }
            relatedVideos = await api.get("videos", {per_page:5, 'content': (result.tags && result.tags.join(' ')) || ""});
        }

        return {
            video: result || [],
            relatedVideos: relatedVideos || [],
        }
    }

    render() {
        return (
            <App title={"نمایش فیلم"}>
                <Header/>
                <VideoSingle {...this.props} />
                <Footer/>
            </App>
        )
    }
}


export default ProgressPage(VideoPage);
