import Document, {Head, Main, NextScript} from 'next/document'
import {ServerStyleSheet} from 'styled-components'
import Loading from '../components/widgets/neccessery/loading'

const description = 'لینگوگپ جامعه آموزش زبان های دوم و مهاجرت می باشد. در این جامعه آموزشگاه های زبان، مراکز اعزام دانشجو، دارالترجمه ها و مهد های دو زبانه پروفایل دارند. همچنین افراد فعال در این حوزه ها هم پروفایل خود را میسازند و قابل جستجو هستند. پیشنهاد های ویژه بسیار زیادی هم در lingogap وجود دارد.'

export default class MyDocument extends Document {


    render() {
        // Polyfill Intl API for older browsers
        const polyfill = `https://cdnjs.cloudflare.com/ajax/libs/babel-polyfill/6.23.0/polyfill.min.js`

        return (
            <html>
            <Head>
                <meta name="viewport" content="width=device-width, initial-scale=1"/>
                <link rel="icon" type="image/png" href="/static/favicon.png"/>
                <link rel="manifest" href="/static/manifest.json"/>

                <meta charSet="UTF-8"/>
                <meta httpEquiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
                <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes"/>

                <meta name="theme-color" content="#db7093"/>
                <meta name="author" content="VOD Web Site"/>
                <meta name="description" content={description}/>

                {/* Open Graph */}
                <link itemprop="url" href="http://styled-components.com/"/>
                <meta itemprop="name" content="VOD Web Site"/>
                <meta itemprop="description" content={description}/>
                <meta itemprop="image" content="/static/atom.png"/>

                {/* Twitter */}
                <meta name="twitter:card" content="summary"/>
                <meta name="twitter:creator" content="@mxstbr"/>
                <meta name="twitter:url" content="http://styled-components.com"/>
                <meta name="twitter:title" content="VOD Web Site"/>
                <meta name="twitter:description" content={description}/>
                <meta name="twitter:image" content="/static/atom.png"/>

                {/* Facebook */}
                <meta property="og:url" content="http://styled-components.com"/>
                <meta property="og:type" content="website"/>
                <meta property="og:title" content="VOD Web Site"/>
                <meta property="og:image" content="/static/atom.png"/>
                <meta property="og:image:height" content="652"/>
                <meta property="og:image:width" content="652"/>
                <meta property="og:description" content={description}/>
                <meta property="og:site_name" content="VOD Web Site"/>


                {/*Styles*/}
                <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet"/>

                {/* inject:css */}<link rel="stylesheet" href="/static/css/main.css" />{/* endinject */}

                {/* inject:head:js */}<script src="/static/js/jquery-3.2.1.min.js"></script>{/* endinject */}

            </Head>
            <body>
            <Loading/>

            <Main />
            <script src={polyfill}/>


            <NextScript />

            {/* inject:js */}<script src="/static/js/main.js"></script> {/* endinject */}
            </body>
            </html>
        )
    }
}
