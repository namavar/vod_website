import App from '../components/App'
import Index from '../components/smart/Posts/Index'
import Header from '../components/smart/Header'
import Footer from '../components/smart/Footer'
import api from "../utils/api";
import Head from 'next/head';

class IndexPage extends React.Component {

    static async getInitialProps({res}) {
        let resultVideos = [];
        let newestVideos = [];
        let bestVideos = [];
        try {
            resultVideos = await api.get("videos");
            newestVideos = await api.get("videos");
            bestVideos = await api.get("videos",{
                rate:1,
                view_count:1,
            });
        } catch (e) {
            console.log(e);
        }
        return {
            videos: resultVideos,
            newestVideos:newestVideos,
            bestVideos:bestVideos
        }

    }

    render() {
        return (
            <App title={'VOD Web Site'}>
                <Head>
                    <link rel="stylesheet" type="text/css" href="/static/css/swiper.min.css"/>
                </Head>
                <Header />
                <Index {...this.props}/>
                <Footer/>
            </App>
        )
    }
}


export default (IndexPage);
