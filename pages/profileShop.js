import App from '../components/App'
import Header from '../components/smart/Header'
import Footer from '../components/smart/Footer'
import React from 'react'
import api from '../utils/api'
import SecurePage from "../components/SecurePage";
import ProgressPage from "../components/ProgressPage";
import ProfileShop from "../components/smart/Posts/ProfileShop";

class ProfileShopPage extends React.Component {

    static async getInitialProps() {
        let products = await api.get("tickets");
        return {
            products: products || [],
        }
    }

  render() {
    return (
      <App title={"پروفایل کاربری | فروشگاه "}>
        <Header/>
        <ProfileShop {...this.props} />
        <Footer/>
      </App>
    )
  }
}


export default ProgressPage(SecurePage(ProfileShopPage));
