import App from '../components/App'
import Header from '../components/smart/Header'
import Footer from '../components/smart/Footer'
import Profile from '../components/smart/Posts/Profile'
import React from 'react'
import api from '../utils/api'
import SecurePage from "../components/SecurePage";
import ProgressPage from "../components/ProgressPage";

class ProfilePage extends React.Component {

    render() {
        return (
            <App title={"پروفایل کاربری "}>
                <Header/>
                <Profile {...this.props} />
                <Footer/>
            </App>
        )
    }
}


export default ProgressPage(SecurePage(ProfilePage));
