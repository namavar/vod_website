import App from '../components/App'
import Header from '../components/smart/Header'
import Footer from '../components/smart/Footer'
import VideoList from '../components/smart/Posts/VideoList'
import React from 'react'
import api from '../utils/api'
import ProgressPage from "../components/ProgressPage"


class VideoListPage extends React.Component {

    static async getInitialProps({query,res, req}) {

        console.log(query);

        let resultVideos = [];
        try {
            if (query.type != 'best' && query.type != 'latest')
                resultVideos = await api.get("videos/" + query.type, {'per_page': 10, view_count: 1,'expand':'video'}, req);
        } catch (e) {
            console.log(e);
        }

        let queryName = '';
        if ( query.type === 'bookmarks' )
            queryName = 'لیست نشان شده ها';
        else if ( query.type === 'buys' )
            queryName = 'لیست خریداری شده ها';
        else if ( query.type === 'latest' )
            queryName = 'لیست آخرین ویدیوها';
        else if ( query.type === 'best' )
            queryName = 'برترین ها';

        switch (query.type) {
            case 'best':
                try {
                    resultVideos = await api.get("videos", {'per_page': 10, view_count: 1,rate:1}, req);
                } catch (e) {

                }
                break;
            case 'latest':
                try {
                    resultVideos = await api.get("videos", {'per_page': 10}, req);
                } catch (e) {

                }
                break;
        }

        return {
            videos: resultVideos,
            queryType: queryName,
            queryTypeEn:query.type
        }

  }


  render() {
    return (
      <App title={"لیست فیلم ها"}>
        <Header/>
        <VideoList {...this.props} />
        <Footer/>
      </App>
    )
  }
}


export default ProgressPage(VideoListPage);
