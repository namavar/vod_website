import App from '../components/App'
import Header from '../components/smart/Header'
import Footer from '../components/smart/Footer'
import Search from '../components/smart/Posts/Search'
import api from '../utils/api'
import ProgressPage from "../components/ProgressPage";

class ListPostPage extends React.Component {


    static async getInitialProps({query, res}) {

        let keyword = (query.keyword && query.keyword.length > 0) ? decodeURI(query.keyword) : null;
        let params = {'per_page': 12,category:''};

        params = (keyword) ? Object.assign({content: keyword}) : params;
        let videoResults = await api.get("videos", params);
        let categories = await api.get("categories");


        return {
            videoResults: videoResults,
            keyword: keyword,
            type: query.type,
            categoryList:categories
        }
    }

    render() {
        let stringTitle = '';
        if (this.props.keyword) {
            stringTitle += 'شما برای واژه ' + this.props.keyword + ' جستجو کرده اید.';
        } else {
            stringTitle = 'نتیجه جستجو همه';
        }

        return (
            <App title={stringTitle}>
                <Header/>
                <Search {...this.props}/>
                <Footer/>
            </App>
        )
    }
}

export default ProgressPage(ListPostPage);
