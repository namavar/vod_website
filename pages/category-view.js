import App from '../components/App'
import Header from '../components/smart/Header'
import Footer from '../components/smart/Footer'
import VideoList from '../components/smart/Posts/VideoList'
import React from 'react'
import api from '../utils/api'
import ProgressPage from "../components/ProgressPage"
import Category from "../components/smart/Posts/Category";


class CategoryView extends React.Component {

    static async getInitialProps({query,res, req}) {

        let resultVideos = [];
        try {
            resultVideos = await api.get("videos", {'per_page': 10, view_count: 1,category:query.cn}, req);
        } catch (e) {
            console.log(e);
        }

        return {
            videos: resultVideos,
            categoryId:query.cn,
            // categoryfaN:query.cn_fa,
        }

  }


  render() {
    return (
      <App title={"لیست فیلم ها"}>
        <Header/>
        <Category {...this.props} />
        <Footer/>
      </App>
    )
  }
}


export default ProgressPage(CategoryView);
