var cluster = require('cluster');
if (cluster.isMaster) {

    // Count the machine's CPUs
    var cpuCount = require('os').cpus().length;

    // Create a worker for each CPU
    for (var i = 0; i < cpuCount; i += 1) {
        cluster.fork();
    }

    // Listen for dying workers
    cluster.on('exit', function (worker) {

        // Replace the dead worker, we're not sentimental
        console.log('Worker %d died :(', worker.id);
        cluster.fork();

    });

// Code to run if we're in a worker process
} else {
        const dev = process.env.NODE_ENV !== 'production'
        const compression = require('compression');
        const moduleAlias = require('module-alias')
        const path = require('path')
        const cache = require('./cache');
        const config = require('./config/config');


        const cookieParser = require('cookie-parser');
        const cookie = require('react-cookie');
        const nextPaths = ['/_next', '/_webpack', '/__webpack_hmr', '/static/']
        const {parse} = require('url');
        const express = require('express');
        const next = require('next')
        const routes = require('./routes')


        const app = next({ dir: '.', dev })
        const handler = routes.getRequestHandler(app)
        const handle = app.getRequestHandler()


        const PORT = process.env.PORT || config.PORT || 3005;
        const server = express();
        server.disable('x-powered-by')



        server.get('/sw.js', (req, res) => {
            res.sendFile(path.resolve('./.next/sw.js'))
        });
        server.get('/favicon.ico', (req, res) => {
            res.sendFile(path.resolve('./static/favicon.ico'))
        });

        app.prepare()
            .then(() => server
                .use(cookieParser())
                .use(acceptToken())
                // .use(cache(app))
                .use(compression())
                .use((req, res) => {
                    if (nextPaths.some(path => req.url.startsWith(path))) return handle(req, res)
                    if (req.path.indexOf('.') > -1) return handle(req, res);

                    cookie.plugToRequest(req, res);
                    return handler(req, res)
                }))
            .then(() => {
                server.listen(PORT, err => {
                    if (err) {
                        throw err
                    }
                    console.log(`> Ready on http://localhost:${PORT}`)
                })
            })


     function acceptToken(options) {
    let self = this;

    return function (req, res, next) {
        const token = req.cookies['token'];

        if (!token) {
            return next();
        }
        // console.log(token);
        return next();
        // apiGetUser(token)
        //     .then((user) => {
        //         req.user = user;
        //         return next();
        //     })
        //     .catch((err) => {
        //         return next(err);
        //         // return res.end(JSON.stringify(err));
        //     });
    }
}

        }