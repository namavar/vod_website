// routes.js
const nextRoutes = require('next-routes')
const routes = module.exports = nextRoutes()

routes.add('index', '/', 'index');
routes.add('myProfile', '/profile', 'profile');
routes.add('myProfileShop', '/profile/shop', 'profileShop');
routes.add('otherProfile', '/profile/:profileId', 'profile');

routes.add('search', '/search/:keyword*', 'search');
routes.add('searchAll', '/search/', 'search');
// routes.add('About', '/about/', 'about');
// routes.add('Rules', '/rules/', 'rules');
// routes.add('Privacy', '/privacy/', 'privacy');

routes.add('VideoListDefault', '/videos', 'video-list');
routes.add('VideoList', '/videos/:type*', 'video-list');
routes.add('CategoryL', '/category/:cn*', 'category-view');

routes.add('VideoSingle', '/v/:videoId/:slug*', 'videoSinglePage');

