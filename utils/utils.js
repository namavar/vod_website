/**
 * Created by miladh on 5/23/17.
 */
import localStorage from 'web-storage'

class Utils {

    ls = localStorage().localStorage;


    getValidateMessage(validMessage) {
        let errors = [];
        for (let kay in validMessage) {
            errors.push(validMessage[kay]);
        }
        return errors;

    }

    setLocalStorage(name, value) {
        this.ls.set(name, JSON.stringify(value));
    }

    getLocalStorage(name) {
        try {
            return JSON.parse(this.ls.get(name));
        }
        catch (er) {
        }
    }

    removeLocalStorage(name) {
        this.ls.remove(name)
    }

    secondToDeurationFormat(sec) {
        if(!sec || isNaN(sec)) return 0;
        let date = new Date(null);
        date.setSeconds(sec); // specify value for SECONDS here
        if (sec < 3600)
            return date.toISOString().substr(11, 8).replace("00:", "");
        return date.toISOString().substr(11, 8)
    }

    toHHMMSS(str) {
        let sec_num = parseInt(str, 10); // don't forget the second param
        let hours = Math.floor(sec_num / 3600);
        let minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        let seconds = sec_num - (hours * 3600) - (minutes * 60);

        if (hours < 10) {
            hours = "0" + hours
        }
        if (minutes < 10) {
            minutes = "0" + minutes
        }
        if (seconds < 10) {
            seconds = "0" + seconds
        }
        return minutes + ':' + seconds
    }

    closeSearch() {
        $(document).ready(() => {
            $('.searchresult,.c_darkoverlay').removeClass('show');
            $('#search_btn i').text('search');
        })
    }

    toPersianNum(num, dontTrim) {

        let i = 0;

        dontTrim = dontTrim || false;
        num = dontTrim ? num.toString() : num.toString().trim();
        let len = num.length;
        let res = '';
        let pos;

        let persianNumbers = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];

        for (; i < len; i++)
            if (( pos = persianNumbers[num.charAt(i)] ))
                res += pos;
            else
                res += num.charAt(i);

        return res;
    }

    arrayUnique(array) {
        let a = array.concat();
        for (let i = 0; i < a.length; ++i) {
            for (let j = i + 1; j < a.length; ++j) {
                if (a[i] === a[j])
                    a.splice(j--, 1);
            }
        }

        return a;
    }

    eliminateDuplicates(data) {

        let result = [];
        data.forEach(function (element, index) {

            // Find if there is a duplicate or not
            if (data.indexOf(element, index + 1) > -1) {

                // Find if the element is already in the result array or not
                if (result.indexOf(element) === -1) {
                    result.push(index);
                }
            }
        });

        return result;
    }


    mobileValidate(mobile) {
        if (mobile.length < 11) {
            return 'شماره تلفن خود را کامل وارد کنید';
        }

        return false
    }

    myTrimToDash(x) {
        return x.replace(/ /g, '-');
    }

}

export default new Utils()
