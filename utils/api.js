import "isomorphic-fetch";
import {getToken} from "../services/AuthService";
import config from "../config/config";
import AuthService from '../services/AuthService';
import Router from '../routes';

const SERVER = config.API;
const checkStatus = res => {
    if (res.status >= 200 && res.status < 300) {
        return res;
    }

    return getJson(res).then(error => Promise.reject(Object.assign(error, {statusCode: res.status})));
};
const getJson = res => res.json();
let TOKENCOOKIE = null;
let transitionTo = Router.transitionTo;

const queryParams = (params) => {
    return Object.keys(params)
        .map(k => encodeURIComponent(k) + '=' + encodeURIComponent(params[k]))
        .join('&');
}
const myFetch = (url, options = {}, req = null) => {
    options = {
        // your default options
        credentials: 'same-origin',
        redirect: 'error',
        ...options,
    };

    let token = AuthService.getToken();

    if (!token && req)
        token = req.cookies['token'];

    if (token) {
        options.headers = Object.assign(options.headers || {}, {'Authorization': 'Bearer ' + token})
    }

    if (options.queryParams) {
        url += (url.indexOf('?') === -1 ? '?' : '&') + queryParams(options.queryParams);
        delete options.queryParams;
    }
    return fetch(url, options);
}

const API = {

    get(url, body, req){
        return myFetch(SERVER + url, {
            method: "GET",
            headers: {
                'Accept': 'application/json',
            },
            queryParams: body,
        }, req)
            .then(checkStatus)
            .then((res) => res.text())
            .then((text) => text.length ? JSON.parse(text) : {})
    },

    post(url, body){
        return myFetch(SERVER + url, {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(body),
        })
            .then(checkStatus)
            .then((res) => res.text())
            .then((text) => text.length ? JSON.parse(text) : {})
    },

    put(url, body){
        return myFetch(SERVER + url, {
            method: "PUT",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(body),
        })
            .then(checkStatus)
            .then((res) => res.text())
            .then((text) => text.length ? JSON.parse(text) : {})
    },

    delete(url, body){
        return myFetch(SERVER + url, {
            method: "DELETE",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(body),
        })
            .then(checkStatus)
            .then((res) => res.text())
            .then((text) => text.length ? JSON.parse(text) : {})
    },


    uploadImage (url, image) {
        let formData = new FormData();
        formData.append('file', image);
        return myFetch(SERVER + url, {
            method: 'POST',
            body: formData
        }).then(checkStatus);
    }

};

export default API;
