/**
 * Created by miladh on 6/2/17.
 */
import Router from 'next/router'

export default (
  ctx,
  to = `/?next=${encodeURIComponent(ctx.req ? ctx.req.url : ctx.pathname)}`
) => {
  if (ctx.res) {
    ctx.res.writeHead(302, { Location: to })
    ctx.res.end()
    return {}
  } else {
    return Router.push(to)
  }
}
