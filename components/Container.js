/**
 * Created by miladh on 6/1/17.
 */
import React from 'react';
import Head from './DefaultHead';

export default (props) => {
    return (
        <div id="container">
            <Head seo={props}/>
            {props.children}
        </div>
    );
};
