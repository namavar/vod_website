/**
 * Created by miladh on 6/2/17.
 */
import React from 'react'
import Router from 'next/router'
import  Auth from '../services/AuthService'
import redirect from '../utils/redirect'

export default (Page) => {
  return class SecurePage extends React.Component {
    static async getInitialProps (ctx) {
      try {
        const  user  = await Auth.loadUser(ctx)
        if (!user) {

          return redirect(ctx)
        }
        let initialProps = {}
        if (Page.getInitialProps) {
          initialProps = await Page.getInitialProps({ ...ctx, user })
        }
        return { user, ...initialProps }
      } catch (e) {
        throw e
      }
    }
    constructor (props) {
      super(props)
    }

    render () {
      return <Page {...this.props} />
    }
  }
}
