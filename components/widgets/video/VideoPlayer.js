import React, {Component} from 'react';
import config from "../../../config/config";

class VideoPlayer extends Component {

    constructor(props) {
        super(props);

    }

    id = '485438kk';
    container = null;

    componentDidMount() {
        setTimeout(() => {
            this.setup();
        })
    }


    componentWillReceiveProps(nextProps) {
        if (nextProps.file !== this.props.file) {
            this.player.setup(nextProps);
        }
    }

    setup(props = this.props) {
        const isBrowser = typeof window !== 'undefined';
        if (isBrowser) {
            const {
                // unfold out all the events
                onViewable, onPlaylist, onPlaylistItem, onPlaylistComplete, onBufferChange,
                onPlay, onPause, onBuffer, onIdle, onComplete, onFirstFrame, onError,
                onPlaybackRateChanged, onSeek, onSeeked, onTime, onMute, onVolume,
                onFullscreen, onResize, onLevels, onLevelsChanged, onVisualQuality,
                onAudioTracks, onAudioTrackChanged, onCaptionsList, onCaptionsChanged,
                onControls, onDisplayClick, onMeta,
                // the rest are configuration options.
                ...setupConfig,
            } = props;

            const events = {
                viewable: onViewable,
                playlist: onPlaylist,
                playlistItem: onPlaylistItem,
                playlistComplete: onPlaylistComplete,
                bufferChange: onBufferChange,
                play: onPlay,
                pause: onPause,
                buffer: onBuffer,
                idle: onIdle,
                complete: onComplete,
                firstFrame: onFirstFrame,
                error: onError,
                playbackRateChanged: onPlaybackRateChanged,
                seek: onSeek,
                seeked: onSeeked,
                time: onTime,
                mute: onMute,
                volume: onVolume,
                fullscreen: onFullscreen,
                resize: onResize,
                levels: onLevels,
                levelsChanged: onLevelsChanged,
                visualQuality: onVisualQuality,
                audioTracks: onAudioTracks,
                audioTrackChanged: onAudioTrackChanged,
                captionsList: onCaptionsList,
                captionsChanged: onCaptionsChanged,
                controls: onControls,
                displayClick: onDisplayClick,
                meta: onMeta,

            };


            // window.jwplayer.key = "Ak0yoIHCH34vPpgGDjeCYETqmxw3/YlsE9a1GQvToS/pdnAb" //config.JWPLAYERKEY;
            if(window && window.jwplayer){
                window.jwplayer.key = config.JWPLAYERKEY;
                this.player = window.jwplayer(this.id).setup(setupConfig);
                this.bindEvents(events);
            }

        }
    }

    bindEvents(events = {}) {
        for (const eventName of Object.keys(events)) {
            const listener = events[eventName];
            this.player.on(eventName, listener);
        }
    }

    render() {
        return (
            <div
                id={this.id}
                ref={(ref) => {
                    this.container = ref;
                }}
            ></div>
        );
    }
}

export default VideoPlayer;