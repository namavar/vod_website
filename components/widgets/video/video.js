/**
 * Created by miladh on 5/27/17.
 */
import React from 'react'
import {Link, Router} from '../../../routes'
import PersianNumber from "../../widgets/input/PersianNumber";
import utils from '../../../utils/utils';

class Video extends React.Component {

    constructor(props) {
        super(props)
        const {type, video} = this.props;
    }

    render() {
        const {type, video} = this.props;
        if ( video !== null )
        {
            let smallImage = (video.image instanceof Object)? video.image.small : video.image;
            if (type === 'grid')
                return <div className="swiper-slide eachuser">
                    <div className='thumbnail slide-blk'>

                        <div className="slide-img ">
                            <Link prefetch route='VideoSingle' params={{videoId: video.username, slug:video.name}} title={video.name}>
                                <a className="block"><img src={smallImage || "../static/img/placeholder.png"} alt=""/></a>
                            </Link>
                        </div>
                        <div className="slide-det">
                            <div className="slide-det-t">
                                <PersianNumber
                                    className="pull-left">{utils.secondToDeurationFormat(video.duration)}</PersianNumber>
                                <ul>
                                    <li>
                                        <i className="material-icons">&#xE8DC;</i>
                                        <PersianNumber
                                            className="num">{video.like_count}</PersianNumber>
                                        <div className="clear"></div>
                                    </li>
                                    <li>
                                        <i className="material-icons">&#xE8F4;</i>
                                        <PersianNumber
                                            className="num">{video.view_count}</PersianNumber>
                                        <div className="clear"></div>
                                    </li>
                                </ul>
                                <div className="clear"></div>
                            </div>
                            <div className="slide-desc">
                                <Link prefetch route='VideoSingle' params={{videoId: video.username, slug:video.name}} title={video.name}><a
                                    className="block">{video.name}</a></Link>
                            </div>
                        </div>

                    </div>
                </div>;
            else
                return <div className='vlist-row'>

                    <div className="vlist-img">
                        <Link className="block cpointer" prefetch route='VideoSingle' params={{videoId: video.username, slug:video.name}}
                              title={video.name}>
                            <a className="block">
                                <img src={video.image || "../../static/img/placeholder.png"} alt=""/>
                            </a>
                        </Link>
                    </div>
                    <div className="vlist-det">
                        <div className="vlist-det-t">
                            <Link className="cpointer" prefetch route='VideoSingle' params={{videoId: video.username, slug:video.name}}
                                  title={video.name}><a className="block">{video.name}</a></Link>
                        </div>
                        <div className="slide-det-t">

                            <ul>
                                <li>
                                    <i className="material-icons">&#xE8DC;</i>
                                    <PersianNumber
                                        className="num">{video.like_count}</PersianNumber>
                                    <div className="clear"></div>
                                </li>
                                <li>
                                    <i className="material-icons">&#xE8F4;</i>
                                    <PersianNumber
                                        className="num">{video.view_count}</PersianNumber>
                                    <div className="clear"></div>
                                </li>
                            </ul>
                            <div className="clear"></div>
                        </div>

                    </div>

                </div>
        }
        else
            return null

    }
}

export default Video;
