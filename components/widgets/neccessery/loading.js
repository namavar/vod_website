/**
 * Created by miladh on 5/27/17.
 */
import React from 'react';

class Loading extends React.Component {

  constructor(props) {
    super(props)
  }

  render() {

    return (
        <div className="mainloader active">
          <div className="topsec"></div>
          <div className="bottomsec"></div>
          <div className="inner">
            <img src="/static/img/logo.png" alt="Logo"/>
          </div>
          <div className="lingodes">
            <span>VOD</span>
          </div>
        </div>
      )
  }
}

export default Loading;
