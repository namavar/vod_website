/**
 * Created by miladh on 6/17/17.
 */
import React from "react";
import ReactPaginate from './PaginationBoxView';
import cx from '../../../utils/classnames'

export default class Pager extends  React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    const {pages, current, onPageChange, disabled} = this.props;
    return (
      (pages>1) && <div className="pager-container">
        <ReactPaginate
          pageNum={pages}
          pageRangeDisplayed={1}
          marginPagesDisplayed={1}
          previousLabel="❮"
          nextLabel="❯"
          breakLabel={<a>&hellip;</a>}
          forceSelected={current - 1}
          containerClassName={cx('pagination', {'disabled': disabled})}
          disabledClassName="disabled"
          activeClassName="active"
          clickCallback={(pageObject)=> {
            onPageChange(pageObject.selected+1)
          }}
        />
      </div>
    )
  }
}
Pager.propTypes = {
  pages: React.PropTypes.number.isRequired,
  current: React.PropTypes.number.isRequired,
  onPageChange: React.PropTypes.func
};
