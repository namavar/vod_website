/**
 * Created by miladh on 5/27/17.
 */
import React from 'react';

class ValidationError extends React.Component {

  constructor(props) {
    super(props)
  }

  render() {
    let Iprops = {...this.props};
    return (
      (Iprops.message) ?
        <span className="ai" {...Iprops}>
                        <i className="material-icons">&#xE001;</i>
          {Iprops.message}
        </span>

        : null

    )
  }
}

export default ValidationError;
