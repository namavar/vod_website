import React from 'react';

export default class LoadingButton extends React.Component {
  render() {
    const { className, loading, ...elProps } = this.props;
    return (
      <button disabled={!!loading} className={className + " " + ((loading)? 'loader' : '')} {...elProps}>
        {this.props.children}
      </button>
    );
  }
}
