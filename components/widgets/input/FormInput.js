/**
 * Created by miladh on 5/27/17.
 */
import React from 'react';

class FormInput extends React.Component {

    constructor(props, context){
        super(props)
    }


    render() {
        let inputProps = {...this.props};
        return (
            <div>
                <div
                    className={"item-input item-input-field " + inputProps.className + ((inputProps.hasError) ? ' input_error' : '')}>
                    <input type="text" {...this.props} />
                </div>
                {
                    inputProps.hasError ?
                        <span className="ai color-red size-10">{inputProps.errorMessage}</span> : ''
                }
            </div>
        );
    }
}

export default FormInput;