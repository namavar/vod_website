/**
 * Created by miladh on 5/27/17.
 */
import React, { PropTypes, Component } from 'react';


const latinToPersianMap = ['۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹', '۰'];
const persianToLatinMap = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0'];
const latinNumbers = [/1/g, /2/g, /3/g, /4/g, /5/g, /6/g, /7/g, /8/g, /9/g, /0/g];
const persianNumbers = [/۱/g, /۲/g, /۳/g, /۴/g, /۵/g, /۶/g, /۷/g, /۸/g, /۹/g, /۰/g];


function latinToPersian(string) {
    let result = string;

    for (let index = 0; index < 10; index++) {
        result = result.replace(latinNumbers[index], latinToPersianMap[index]);
    }

    return result;
}

function persianToLatin(string) {
    let result = string;

    for (let index = 0; index < 10; index++) {
        result = result.replace(persianNumbers[index], persianToLatinMap[index]);
    }

    return result;
}



class PersianNumberInput extends React.Component {

    convert(string) {
        // return latinToPersian(string);
        return persianToLatin(string);
    }

    modelValue = '';

    render () {
        let inputProps = {...this.props};
        return (
        <input {...inputProps}
            onKeyPress={(e)=>{
              let theEvent = e || window.event,
                key = theEvent.keyCode || theEvent.which,
                exclusions=[8,9]; /*Add exceptions here */
              if(exclusions.indexOf(key)>-1){return;}
              if(persianToLatinMap.indexOf(e.key)<0 && latinToPersianMap.indexOf(e.key)<0 && e.key!='Enter') e.preventDefault(); inputProps.onKeyPress && (inputProps.onKeyPress(e))}}
                   onChange={(e) => {$(e.target).val(latinToPersian(e.target.value)); e.target.modelValue=persianToLatin(e.target.value); inputProps.onChange && (inputProps.onChange(e)) }}/>
        );
    }
}

export default PersianNumberInput;
