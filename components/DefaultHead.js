/**
 * Created by miladh on 6/1/17.
 */
import React from 'react';
import Head from 'next/head';
import NProgress from 'nprogress';
import Router from 'next/router';

Router.onRouteChangeStart = (url) => {
    NProgress.start();
};
Router.onRouteChangeComplete = () => NProgress.done();
Router.onRouteChangeError = () => NProgress.done();

export default (props) => {
    let {seo} = props;
    seo = seo || {};

    return  <Head>
        <title>{seo.title || 'LingoGap'}</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width"/>

        <meta name="description" content={seo.description}/>
        <meta name="keywords" content={seo.keywords}/>
        <link rel="canonical" href="https://lingogap.com"/>
        <link rel="next" href="https://lingogap.com/about/"/>
        <meta property="og:locale" content="fa_IR"/>
        <meta property="og:type" content="website"/>
        <meta property="og:title" content={seo.ogTitle} />
        <meta property="og:description" content={seo.ogDescription} />
        <meta property="og:url" content="https://lingogap.com"/>
        <meta property="og:site_name" content={seo.ogSiteName} />


        {props.children}

    </Head>
};
