/**
 * Created by miladh on 6/2/17.
 */
import React from 'react'
import Router from 'next/router'
import NProgress from 'nprogress'

export default (Page) => {
  return class ProgressPage extends React.Component {

    static async getInitialProps (ctx) {
      try {
        let initialProps = {}
        if (Page.getInitialProps) {
          initialProps = await Page.getInitialProps({ ...ctx })
        }
        return {...initialProps}
      } catch (e) {
        throw e
      }

      Router.onRouteChangeStart = () => NProgress.start();
      Router.onRouteChangeComplete = () => NProgress.done()
      Router.onRouteChangeError = () => NProgress.done()

    }

    constructor (props) {
      super(props)
    }

    render () {
      return <Page {...this.props} />
    }
  }
}
