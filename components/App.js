import React from 'react';
import Link from 'next/link';
import Router from 'next/router';
import Container from './Container';
import withAnalytics from './analytics/withAnalytics';
import Loading from "../components/widgets/neccessery/loading";


let Page = (props) => (
    <Container {...props}>
        {props.children}
    </Container>
);


Page = withAnalytics(Page);

export default Page;
