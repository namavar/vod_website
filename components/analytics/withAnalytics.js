/**
 * Created by miladh on 6/1/17.
 */
import { Component } from 'react';
import { loadGetInitialProps } from 'next/dist/lib/utils';

let ReactGA;
if (process.browser) {
  ReactGA = require('react-ga'); // eslint-disable-line global-require
}

export default ComposedComponent => class WithAnalytics extends Component {
  static async getInitialProps(ctx) {
    return loadGetInitialProps(ComposedComponent, ctx);
  }

  constructor(props) {
    super(props);
    if (process.browser) {
      if (process.env.NODE_ENV === 'production') {
        ReactGA.initialize('UA-92703151-1');
      }
    }
  }

  componentDidMount() {
    const page = window.location.pathname;
    ReactGA.set({ page });
    ReactGA.pageview(page);
  }

  render() {
    return <ComposedComponent {...this.props} />;
  }
};
