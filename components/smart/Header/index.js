import React from 'react';
import AuthService from '../../../services/AuthService'
import ValidationError from "../../widgets/input/ValidationError";
import PersianNumberInput from "../../widgets/input/PersianNumberInput";
import Auth from "../../../services/AuthService";
import utils from '../../../utils/utils';
import api from '../../../utils/api';
import {Link, Router} from '../../../routes'
import LoadingButton from "../../widgets/input/LoadingButton";
import PersianNumber from "../../widgets/input/PersianNumber";
import Suggestions from "./Suggestions";
import client from "../../../services/Events";

const LOGIN_INIT = "LOGIN_INIT";
const LOGIN_CODE = "LOGIN_CODE";
const REGISTER_INIT = "REGISTER_INIT";
const REGISTER_CODE = "REGISTER_CODE";
const RESEND_SECOND = 60;

class Header extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            mobile: null,
            name: null,
            lastname: null,
            activeCode: null,
            active: {},
            loginMessage: {},
            activeMessage: {},
            registerMessage: {},
            reSendTime: 0,
            reSendActive: false,
            isAuthenticated: null,
            LS: LOGIN_INIT,
            RS: REGISTER_INIT,
            showPrevResult: false,
            prevItems: [],
            loadingcodereq: false,
            loadingregreq: false,
            resetcodeload: false,
            showResult: false,
            bookmark_require_login: false,
            categoryList: []
        };

        this.login = this.login.bind(this);
        this.resend = this.resend.bind(this);
        this.register = this.register.bind(this);
        this.active = this.active.bind(this);
        this.logout = this.logout.bind(this);
        this.countDown = this.countDown.bind(this);
        this.runJs = this.runJs.bind(this);
        this.changeSearch = this.changeSearch.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }


    componentDidUpdate() {
        this.runJs();
    }

    componentWillReceiveProps(nextProps) {
        this.runJs();
    }

    componentDidMount() {

        // this.runJs();
        // Re-render for isomorphic purposes
        requestAnimationFrame(() => {
            this.setState({appIsMounted: true});
            this.state.searchKeyword = "";
            $('.searchMokhal').find('input').val('');
            $('.search_prev_result').removeClass('active');

            // Do whatever you did in your old componentDidMount here
        });

        this.getCategories();


    }

    runJs() {
        setTimeout(() => {
            $('#login_btn').click(function () {
                if ($('#loginForm')[0])
                    $('#loginForm')[0].reset();
                $('#login_modal').addClass('active');
                setTimeout(() => {
                    $('#headerLoginMobile').focus();
                }, 300)
            });
            $('#download_btn').click(function () {
                $('#download_modal').addClass('active');
                // setTimeout(() => {
                //     $('#headerLoginMobile').focus();
                // }, 300)
            });

            $('#like_btn').click(function () {
                $('#login_modal .ttl_icon').html('برای لایک کردن ابتدا وارد شوید');
            });
            $('.profile_box #login_btn').click(function () {
                $('#login_modal .ttl_icon').html('ورود به سایت');
            });
            $('#bookmark_btn').click(function () {
                $('#login_modal .ttl_icon').html('برای نشان کردن ابتدا وارد شوید');
            });
            $('.c_modal_overlay').click(function () {
                $('.c_modal').removeClass('active');
            });

            $('.close_modal').click(function () {
                $('.c_modal').removeClass('active');
            });
            $('.menu-btn').click(function(){
                $('.secondary_header menu ul,.back-dark').toggleClass('active');
            });
            $('.back-dark').click(function(){
                $('.secondary_header menu ul,.back-dark').removeClass('active');
            });
            $('#bookmark_modal').click(function () {
                $('#login_modal,.c_modal_overlay').addClass('active');
            })

        }, 200)
    }

    getCategories() {
        api.get("categories").then((res)=> {
            this.setState({categoryList:res});
            this.runJs();
        });
    }

    handleChange(type, e) {
        this.setState({activeMessage: {}})
        this.setState({registerMessage: {}})
        this.setState({loginMessage: {}})
        let value = e.target.modelValue || e.target.value;
        this.setState({[type]: value});
    }

    login(e) {
        e.preventDefault();
        this.setState({loginMessage: {}, loadingcodereq: true, resetcodeload: true})
        Auth.login({mobile: this.state.mobile}).then((res) => {
            this.setState({active: res, loadingcodereq: false, resetcodeload: false});
            this.setState({LS: LOGIN_CODE});
            this.countDown(RESEND_SECOND)
            setTimeout(() => {
                $('#headerActiveCodeInput').focus();
            }, 300)
        }, (err) => {
            this.setState({loginLoading: false})
            if (err.statusCode == 422)
                this.setState({loginMessage: err, loadingcodereq: false, resetcodeload: false})
            else if (err.statusCode == 401)
                this.setState({
                    LS: REGISTER_INIT
                });
            setTimeout(() => {
                $('#indexRegisterName').focus();
            }, 300)

        })
    }

    resend(e) {
        e.preventDefault();
        this.setState({resetcodeload: true})
        Auth.resendCode(this.state.active.id, this.state.active.security_code).then((res) => {
            this.setState({resetcodeload: false, reSendActive: false});
            this.countDown(RESEND_SECOND)
        }, (err) => {
            this.setState({resetcodeload: false})
        })
    }

    register(e) {
        e.preventDefault();
        this.setState({registerMessage: {}, loadingregreq: true})
        Auth.signup(this.state.mobile, this.state.name, this.state.lastname).then((res) => {
            this.setState({active: res, loadingregreq: false, LS: LOGIN_CODE});
            this.countDown(RESEND_SECOND)
        }, (err) => {
            this.setState({registerMessage: err, loadingregreq: false})
        })
    }

    logout() {
        AuthService.logout();
    }

    active(e) {
        e.preventDefault();
        this.setState({activeMessage: {}, loadingcodereq: true, loadingregreq: true})
        Auth.activeCode(Object.assign({code: this.state.activeCode}, this.state.active)).then((res) => {
            localStorage.clear();
            AuthService.saveUserWithToken(res);
            Router.pushRoute('myProfile')

            // $('#login_modal').removeClass('active');
            // $('#register_modal').removeClass('active');
            // client.emit("LOGIN",{})
            // super.forceUpdate();
            // this.setState({loadingcodereq: false, loadingregreq: false})
        }, (err) => {
            this.setState({activeMessage: err, loadingcodereq: false, loadingregreq: false})
        })
    }

    countDown(second) {
        this._interval = setInterval(() => {
            if (second > 0) {
                this.setState({reSendTime: utils.toHHMMSS(second)})
                second--;
            } else {
                this.setState({reSendActive: true})
                clearInterval(this._interval)
            }
        }, 1000);
    }


    changeSearch(e) {
        this.setState({searchKeyword: e.target.value || ""})
        let promises = [];

        if (e.target.value.length > 3) {
            promises.push(api.get('autocompletes', {content: e.target.value}))

            Promise.all(promises).then((res) => {
                console.log(res)
                this.state.showPrevResult = true;
                this.state.prevItems = (res[0].items || []);
                this.setState({prevItems: this.state.prevItems});
                this.runJs();
                $(document).ready(() => {
                    $('body').keyup((e) => {
                        if (e.keyCode == 13) {
                            this.closeSearchBar();
                        }
                    })
                    $('.submitsearchicon').click(() => {
                        this.closeSearchBar();
                    })
                })
            })
        } else {
            this.state.showPrevResult = false;
            this.setState({showPrevResult: this.state.showPrevResult});
        }
    }

    render() {
        return (


            // <ul className="Header-Menu">
            //   <li><Link href="/" activeClassName='active' ><a>Home</a></Link></li>
            //   {!this.props.user && (<li><Link href="/login" activeClassName='active' ><a>Login</a></Link></li>)}
            //   {this.props.user && (<li><Link href="/create" activeClassName='active' ><a>Create Post</a></Link></li>)}
            //   {this.props.user && (<UserDropdown user={this.props.user}/>)}
            // </ul>


            <header>


                {/*Login Modal*/}
                {!Auth.isAuthenticated() && <div className="c_modal" id="login_modal">
                    <div className="c_modal_overlay"></div>
                    <div className="c_modal_inner">
                        <div className="c_modal_header">
                            <a className="close_modal">
                                <i className="material-icons">&#xE5CD;</i>
                            </a>
                        </div>
                        <div className="c_modal_content">
                            {/*Before Send*/}
                            {
                                (this.state.LS == LOGIN_INIT) &&
                                <form id="loginForm" className="send_code" onSubmit={this.login} name="login">


                                    {this.state.bookmark_require_login ?
                                        <img src="/static/img/inf.png" className="icon_img"
                                             alt="Image Icon Login">

                                        </img>
                                        :

                                        <img src="/static/img/login.png" className="icon_img"
                                             alt="Image Icon Login">

                                        </img>
                                    }
                                    {this.state.bookmark_require_login ?

                                        <div className="ttl_icon">ورود به سایت</div>

                                        :

                                        <div className="ttl_icon">برای نشان کردن ابتدا وارد شوید</div>

                                    }
                                    <div className="text">
                                        برای ورود به نرم افزار ابتدا شماره تلفن خود را وارد نمایید تا "کدورود"
                                        برایتان ارسال شود
                                    </div>
                                    <PersianNumberInput
                                        className={"margin-top-50 number-input " + ((this.state.loginMessage.mobile) ? 'error' : '')}
                                        size="11"
                                        id="headerLoginMobile"
                                        autoFocus
                                        style={{direction: 'ltr'}}
                                        maxLength="11" type="text"
                                        onChange={this.handleChange.bind(this, 'mobile')}
                                        placeholder=" شماره تلفن"/>
                                    <ValidationError message={this.state.loginMessage.mobile}/>
                                    <div>
                                        <LoadingButton loading={this.state.loadingcodereq} type="submit"
                                                       className="btn_nice_grad margin-top-30 ">
                                            ارسال کد
                                        </LoadingButton>
                                    </div>
                                </form>
                            }
                            {
                                (this.state.LS == LOGIN_CODE) &&
                                <form className="send_code" name="active" onSubmit={this.active}>
                                    <img src="/static/img/confirmation.png"
                                         className="icon_img margin-bottom-20" alt="Image Icon Login"></img>
                                    <div className="ttl_icon margin-bottom-20">وارد
                                        ورود</div>
                                    <div className="textContnet">
                                        <p> کد ورود به شماره تلفن<PersianNumber className="phonenumber">{this.state.mobile}</PersianNumber> ارسال گردید پس از دریافت پیامک حاوی
                                            کد ورود، نرم افزار به صورت خودکار وارد مرحله بعد می شود.</p>
                                        <p>در غیر این صورت کد را در بخش زیر وارد کرده و دکمه فعال سازی را
                                            بفشارید.</p>
                                    </div>
                                    <div className="phone-fileds margin-bottom-30 margin-top-50">
                                        <div className="ltr inline-block ">
                                            <PersianNumberInput type="tel" placeholder="*****"
                                                                maxLength="5"
                                                                id="headerActiveCodeInput"
                                                                autoFocus
                                                                style={{direction: 'ltr'}}
                                                                className={((this.state.activeMessage.code) ? 'error' : '')}
                                                                onChange={this.handleChange.bind(this, 'activeCode')}/>
                                        </div>
                                        <ValidationError message={this.state.activeMessage.code}/>
                                    </div>
                                    <LoadingButton loading={this.state.loadingregreq} type="submit"
                                                   className="btn_nice_grad">
                                        ورود
                                    </LoadingButton>
                                    <div className="clear"></div>
                                    <LoadingButton loading={this.state.resetcodeload} type="button"
                                                   onClick={this.resend}
                                                   disabled={!this.state.reSendActive} value="ارسال مجدد کد"
                                                   className="btn_nice_grad_gray">
                                        ارسال مجدد کد
                                        <span className="countdown margin-right-10">{!this.state.reSendActive &&
                                        <PersianNumber>&nbsp;{this.state.reSendTime}</PersianNumber>}</span>
                                    </LoadingButton>
                                </form>
                            }

                            {
                                (this.state.LS == REGISTER_INIT) &&
                                <form id="registerForm" className="send_code" onSubmit={this.register}>
                                    <img src="/static/img/login.png" className="icon_img"
                                         alt="Image Icon Login"></img>
                                    <div className="ttl_icon">ثبت نام کنید</div>
                                    {/*<div className="textContnet">*/}
                                    {/*<p>برای ورو به نرم افزار ابتدا شماره تلفن خود را وارد نمایید تا  کد ورود برایتان ارسال شود  </p>*/}
                                    {/*</div>*/}
                                    <div className="c_inputs">


                                        <label>
                                            <i className="material-icons">&#xE7FD;</i>
                                            <div className="">
                                                <input type="text" placeholder="نام"
                                                       autoFocus={true}
                                                       className={((this.state.registerMessage.name) ? 'error' : '')}
                                                       onChange={this.handleChange.bind(this, 'name')}/>
                                            </div>
                                            <ValidationError message={this.state.registerMessage.name}/>
                                        </label>


                                        <label>
                                            <i className="material-icons">&#xE7FD;</i>
                                            <div className="">
                                                <input type="text" placeholder="نام خانوادگی"
                                                       className={((this.state.registerMessage.lastname) ? 'error' : '')}
                                                       onChange={this.handleChange.bind(this, 'lastname')}/>
                                                <ValidationError message={this.state.registerMessage.lastname}/>
                                            </div>
                                        </label>

                                        {/*<label>*/}
                                        {/*<PersianNumberInput type="tel" placeholder="شماره تلفن"*/}
                                        {/*maxLength="11"*/}
                                        {/*className={((this.state.registerMessage.mobile) ? 'error' : '')}*/}
                                        {/*onChange={this.handleChange.bind(this, 'mobile')}/>*/}
                                        {/*<ValidationError message={this.state.registerMessage.mobile}/>*/}
                                        {/*</label>*/}
                                    </div>
                                    <LoadingButton loading={this.state.loadingregreq} type="submit"
                                                   className="btn_nice_grad margin-top-30">
                                        ثبت نام
                                    </LoadingButton>
                                </form>
                            }
                            {/*Before Send*/}
                            {/*After Send*/}
                            {/*After Send*/}
                        </div>
                    </div>
                </div>}
                {/*Login Modal*/}


                {/*Login Modal*/}
                <div className="c_modal" id="download_modal">
                    <div className="c_modal_overlay"></div>
                    <div className="c_modal_inner">
                        <div className="c_modal_header">
                            <a className="close_modal">
                                <i className="material-icons">&#xE5CD;</i>
                            </a>
                        </div>
                        <div className="c_modal_content">
                            <div className="dl-modal">
                                <div className="dl-modal-in">
                                    <div className="dl-modal-img"><img src="/static/img/download.png" alt="" /></div>
                                    <div className="dl-modal-t">دانلود ویدئو</div>

                                    <div className="dl-modal-text">
                                        به دلیل رعایت حقوق ناشران، تمامی فیلم های رایگان و اشتراکی VOD فقط بر روی نرم افزار موبایل قابل دانلود است.
                                    </div>
                                    <div>
                                        <a className="btn_gray">دانلود نسخه Android</a>
                                        <a className="btn_gray">دانلود نسخه IOS</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/*Login Modal*/}

                {/*Gift Modal*/}
                <div className="c_modal" id="gift_modal">
                    <div className="c_modal_overlay"></div>
                    <div className="c_modal_inner">
                        <div className="c_modal_header">
                            <a className="close_modal">
                                <i className="material-icons">&#xE5CD;</i>
                            </a>
                        </div>
                        <div className="c_modal_content">
                            <img src="/static/img/gift.png" className="icon_img" />
                            <span className="icon_title">تبریک</span>
                            <span className="text_modal">
                                کد هدیه ۵۰ بلیطی سرویس پیامکلی بر روی حساب شما اعمال خواهد شد.
                            </span>
                            <div className="ModalButtonHolder">
                                <button className="btn_nice_grad block margin-top-10 margin-bottom-5" type="button">دریافت کد هدیه</button>

                            </div>
                        </div>
                    </div>
                </div>
                {/*Gift Modal*/}




                <section className="top_header">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-3 col-sm-3">
                                <div className="logo_holder">
                                    <Link prefetch route='index'>
                                        <a title="logo"><img src="/static/img/logo.png" alt="Logo"/></a>
                                    </Link>
                                </div>
                            </div>
                            <div className="col-md-6 col-sm-5">
                                <div className="top_search">


                                    <form className="searchMokhal">
                                        <Suggestions />

                                    </form>

                                </div>
                            </div>
                            <div className="col-md-3 col-sm-4">
                                <div className="actions_holder">
                                    {/*Logged IN*/}
                                    {/*Logged IN*/}
                                    {/*Guest*/}
                                    {/*Guest*/}

                                    {this.renderLoginSection()}

                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section className="secondary_header">
                    <div className="container">
                        <button className="menu-btn">
                            <i className="material-icons">&#xE5D2;</i>
                        </button>
                        <div className="back-dark"></div>
                        <menu>
                            <ul>
                                <li>
                                    <input type="checkbox" name="chk1" />
                                    <Link prefetch route='index'>
                                        <a>خانه</a>
                                    </Link>
                                </li>
                                <li className="has-submenu">
                                    <input type="checkbox" name="chk1" />
                                    <Link>
                                        <a>دسته بندی ها </a>
                                    </Link>
                                    <div className="sub-menu">
                                        <div className="container">
                                            <ul>

                                                {this.state.categoryList && this.state.categoryList.map((item , index)=> {

                                                    return  (
                                                        <li key={index}>
                                                            <Link prefetch route="CategoryL" params={{cn:item.name_latin}}>
                                                                <a>{item.name}</a>
                                                            </Link>
                                                        </li>
                                                    )

                                                })}
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <input type="checkbox" name="chk1" />
                                    <Link prefetch route="VideoList" params={{type:'best'}}>
                                        <a>برترین ها</a>
                                    </Link>
                                </li>
                                <li>
                                    <input type="checkbox" name="chk1" />
                                    <Link prefetch route="VideoList" params={{type:'bookmarks'}}>
                                        <a>نشان شده ها</a>
                                    </Link>
                                </li>
                            </ul>
                        </menu>
                    </div>
                </section>
            </header>

        );
    }

    renderLoginSection() {
        if (this.state.appIsMounted) {
            return AuthService.isAuthenticated() ?
                (
                    <ul>
                        {/*<li className="profile_box">*/}
                        {/*<div className="actions_inner">*/}
                        {/*<i className="icon material-icons">account_circle</i>*/}
                        {/*<span>*/}
                        {/*<span>{AuthService.getUser().name + ' ' + AuthService.getUser().lastname}</span>*/}
                        {/*<span> خوش آمدید</span>*/}
                        {/*</span>*/}
                        {/*</div>*/}
                        {/*</li>*/}
                        <li className="logout">
                            <div className="actions_inner">
                                <i className="cpointer icon material-icons">&#xE879;</i>
                                <a href="" onClick={this.logout} title="خروج از حساب کاربری"><span>خروج</span></a>
                            </div>
                        </li>
                        <li className="profile_box">
                            <div className="actions_inner">
                                <Link prefetch route="myProfile">
                                    <a id="login_btn" className="login_btn likebtn">مشاهده پروفایل</a>
                                </Link>
                                <Link prefetch route="myProfile"><i
                                    className="icon material-icons">account_circle</i></Link>
                            </div>
                        </li>
                    </ul>
                )
                :
                (
                    <ul>
                        <li className="profile_box">
                            <div className="actions_inner">
                                    <a id="login_btn" className="login_btn likebtn">ورود و ثبت نام</a>
                                <i className="icon material-icons">account_circle</i>
                            </div>
                        </li>
                    </ul>
                )

        } else return null
    }
}

export default Header;
