/**
 * Created by miladh on 6/17/17.
 */
import Autosuggest from 'react-autosuggest';
import api from '../../../utils/api';
import utils from '../../../utils/utils';
import PersianNumber from "../../widgets/input/PersianNumber";
import {Link} from '../../../routes'

// When suggestion is clicked, Autosuggest needs to populate the input element
// based on the clicked suggestion. Teach Autosuggest how to calculate the
// input value for every given suggestion.
const getSuggestionValue = suggestion => suggestion.name;

// Use your imagination to render suggestions.
const renderSuggestion = suggestion => (
    <Link prefetch route='VideoSingle' params={{videoId: suggestion.username}} title={suggestion.name}>
        <a>
            <div className="thumbnail"
                 style={{"backgroundImage": "url(" + (suggestion.image) + ")"}}></div>
            <div className="detail">
                <h2>{suggestion.name}</h2>
                <span className="views">
                <i className="material-icons">&#xE417;</i>
                <PersianNumber>{suggestion.view_count}</PersianNumber>
              </span>
            </div>
        </a>
    </Link>
);


const renderInputComponent = inputProps => (
    <div className="searchHolder">
        <input id="searchText" type="text" {...inputProps} placeholder="جستجو کنید ..."/>
        <Link prefetch
              route={inputProps.value && inputProps.value.length >= 2 ? 'search' : 'searchAll'}
              params={inputProps.value && inputProps.value.length >= 2 && {keyword: utils.myTrimToDash(inputProps.value)}}>
            <button type="submit" className="cpointer">
                <i className="cpointer submitsearchicon material-icons">&#xE8B6;</i>
            </button>
        </Link>


    </div>


);

function renderSuggestionsContainer({containerProps, children, query}) {
    containerProps.className = containerProps.className + " suggestsHolder active"
    return (
        <div {... containerProps}>

            {children}
            {/*// <div>*/}
            {/*Press Enter to search <strong>{query}</strong>*/}
            {/*</div>*/}
        </div>
    );
}


class Suggestions extends React.Component {
    constructor(props) {
        super(props);

        // Autosuggest is a controlled component.
        // This means that you need to provide an input value
        // and an onChange handler that updates this value (see below).
        // Suggestions also need to be provided to the Autosuggest,
        // and they are initially empty because the Autosuggest is closed.
        this.state = {
            value: '',
            suggestions: [],
            boolSearch: true
        };
    }

    onChange = (event, {newValue}) => {
        this.setState({
            value: newValue
        });
    };

    // Autosuggest will call this function every time you need to update suggestions.
    // You already implemented this logic above, so just use it.
    onSuggestionsFetchRequested = ({value}) => {

        // if ( this.state.boolSearch )
        // {
        let promises = [];
        promises.push(api.get('videos', {content: value}))

        Promise.all(promises).then((res) => {

            this.setState({
                suggestions: (res[0].items || [])
            });
        })

        this.state.boolSearch = false;
        // }

        // setTimeout(()=> {
        //   this.state.boolSearch = true;
        // } , 1000 )

    };

    // Autosuggest will call this function every time you need to clear suggestions.
    onSuggestionsClearRequested = () => {
        this.setState({
            suggestions: []
        });
    };

    render() {
        const {value, suggestions} = this.state;

        // Autosuggest will pass through all these props to the input element.
        const inputProps = {
            placeholder: 'مدرس زبان، مترجم، نام موسسه را جستجو کنید...',
            value,
            onChange: this.onChange
        };

        // Finally, render it!
        return (
            <Autosuggest
                suggestions={suggestions}
                onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                onSuggestionSelected={this.props.onSelect || null}
                getSuggestionValue={getSuggestionValue}
                renderSuggestion={renderSuggestion}
                renderInputComponent={renderInputComponent}
                renderSuggestionsContainer={renderSuggestionsContainer}
                inputProps={inputProps}
            />
        );
    }
}

export default Suggestions
