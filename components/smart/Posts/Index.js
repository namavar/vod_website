import React from 'react'
import utils from '../../../utils/utils';
import {Link, Router} from '../../../routes'
import Video from "../../widgets/video/video";
import PersianNumber from "../../widgets/input/PersianNumber";

const LOGIN_INIT = "showlogin";
const LOGIN_CODE = "showenter";
const REGISTER_INIT = "showreg";
const RESEND_SECOND = 60;


class Index extends React.Component {

    constructor(props) {
        super(props)
        this.state = {}
    }

    componentDidMount() {
        const Swiper = require('swiper');
        const swiper_movie = new Swiper(this.refs.movie_swipe, {

            nextButton: '#previns1',
            prevButton: '#nextins1',
            slidesPerView: 'auto',
            autoplay: '2000',
            loop: 'true',
            centeredSlides: true,
            paginationClickable: true,
            spaceBetween: 15,
            breakpoints: {
                768: {slidesPerView: '1', centeredSlides: false}

            }

        });
        const swiper_new_video = new Swiper(this.refs.new_video_swipe, {
            slidesPerView: 5,
            nextButton: '#previns2',
            prevButton: '#nextins2',
            loop: 'false',
            spaceBetween: 25,
            breakpoints: {
                1200: {slidesPerView: 4, spaceBetween: 20},
                768: {slidesPerView: 3, spaceBetween: 20},
                640: {slidesPerView: 1},
                320: {slidesPerView: 1}
            }

        });
        const swiper_new_topvideo = new Swiper(this.refs.new_topvideo_swipe, {
            slidesPerView: 5,
            nextButton: '#previns3',
            prevButton: '#nextins3',
            loop: 'false',
            spaceBetween: 25,
            breakpoints: {
                1200: {slidesPerView: 4, spaceBetween: 20},
                768: {slidesPerView: 3, spaceBetween: 20},
                640: {slidesPerView: 1},
                320: {slidesPerView: 1}
            }


        });

        setTimeout(() => {

            $(document).ready(() => {

                $(function () {
                    $('a[href*="#"]:not([href="#"])').click(function () {
                        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                            var target = $(this.hash);
                            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                            if (target.length) {
                                $('html, body').animate({
                                    scrollTop: target.offset().top
                                }, 500);
                                return false;
                            }
                        }
                    });
                });


            });

        }, 200)

        // Re-render for isomorphic purposes
        requestAnimationFrame(() => {
            this.setState({appIsMounted: true});
            // Do whatever you did in your old componentDidMount here
        });


    }


    render() {
        const {videos,newestVideos,bestVideos} = this.props;
        return (
            <main className="">

                {
                    (videos && videos.items && videos.items.length > 0) &&
                    <section className="top-video">
                        <div className="swiper-container" ref="movie_swipe" id="movie_swipe">
                            <div className="swiper-wrapper">

                                {
                                    videos.items.map((item, index) => {
                                        return (
                                            <div key={index} className="swiper-slide eachads"
                                                 style={{"backgroundImage": "url(" + ( item.image || "/static/img/1.png" ) + ")"}}>
                                                <div className='thumbnail'>
                                                    <Link prefetch route='VideoSingle'
                                                          params={{videoId: item.username, slug: item.name}}
                                                          title={item.title}>
                                                        <a className="block" title={item.title}>
                                                            {/*<div className="slide-img" ></div>*/}
                                                            <div className="slide-desc">
                                                                <div className="slide-desc-l">
                                                <span
                                                    className="size-12 margin-left-10 line-height-32">مشاهده ویدئو</span>
                                                                    <i className="material-icons pull-left size-32">&#xE039;</i>
                                                                </div>
                                                                <div className="slide-desc-r">
                                                                    <h2 className="slide-title">{item.title}</h2>
                                                                    <div className="size-14"><i
                                                                        className="material-icons pull-right margin-left-10">&#xE8B5;</i><span
                                                                        className="line-height-30">
                                                                        <PersianNumber>{utils.secondToDeurationFormat(item.duration)}</PersianNumber>
                                                                    </span></div>
                                                                </div>

                                                                <div className="clear"></div>
                                                            </div>
                                                        </a>
                                                    </Link>
                                                </div>
                                            </div>
                                        )

                                    })

                                }


                            </div>
                            <div className="swiper-button-prev text-left" id="previns1">
                                <i className="material-icons">
                                    &#xE315;
                                </i>
                            </div>
                            <div className="swiper-button-next text-right" id="nextins1">
                                <i className="material-icons">
                                    &#xE314;
                                </i>
                            </div>
                        </div>
                    </section>

                }


                <section className="main-content">
                    <div className="container">
                        <div className="padding-left-35 padding-right-35">
                            {
                                (newestVideos && newestVideos.items && newestVideos.items.length > 0) &&
                                <div className="blk new_video">
                                    <div className="blk-h">
                                        <h2>جدیدترین ویدئو ها
                                            <Link prefetch route="VideoList" params={{type: 'latest'}}><a className="cpointer btn pull-left">مشاهده همه</a></Link>
                                        </h2>
                                    </div>
                                    <div className="blk-c">
                                        <div className="row">
                                            <div className="col-md-12">
                                                <div className="swiper-container" ref="new_video_swipe">
                                                    <div className="swiper-wrapper">
                                                        {
                                                            newestVideos.items.map((item, index) => {
                                                                return (
                                                                    <Video type="grid" video={item} key={index}/>
                                                                )
                                                            })
                                                        }
                                                    </div>
                                                </div>
                                                <div className="swiper-button-prev text-left" id="previns2">
                                                    <i className="material-icons">
                                                        &#xE314;
                                                    </i>
                                                </div>
                                                <div className="swiper-button-next text-right" id="nextins2">
                                                    <i className="material-icons">
                                                        &#xE315;
                                                    </i>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            }


                            {
                                (bestVideos && bestVideos.items && bestVideos.items.length > 0) &&
                                <div className="blk new_video margin-bottom-0">
                                    <div className="blk-h">
                                        <h2>پربازدیدترین ویدئو ها
                                            <Link prefetch route="VideoList" params={{type: 'best'}}><a className="cpointer btn pull-left">مشاهده همه</a></Link>
                                        </h2>
                                    </div>
                                    <div className="blk-c">
                                        <div className="row">
                                            <div className="col-md-12">
                                                <div className="swiper-container" ref="new_topvideo_swipe">
                                                    <div className="swiper-wrapper">
                                                        {
                                                            bestVideos.items.map((item, index) => {
                                                                return (
                                                                    <Video type="grid" video={item} key={index}/>
                                                                )
                                                            })
                                                        }
                                                    </div>

                                                </div>
                                                <div className="swiper-button-prev text-left" id="previns3">
                                                    <i className="material-icons">
                                                        &#xE314;
                                                    </i>
                                                </div>
                                                <div className="swiper-button-next text-right" id="nextins3">
                                                    <i className="material-icons">

                                                        &#xE315;
                                                    </i>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            }


                        </div>
                    </div>
                </section>

            </main>
        )
    }
}

export default Index
