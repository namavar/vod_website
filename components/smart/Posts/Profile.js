import React from 'react'
import api from '../../../utils/api'
import Auth from '../../../services/AuthService'
import PersianNumber from "../../widgets/input/PersianNumber";
import {Link, Router} from '../../../routes'
import config from "../../../config/config";
import Countdown from "../../widgets/Countdown";


const LOGIN_INIT = "showlogin";
const Completionist = () => <span>زمان بسته به اتمام رسیده!!!</span>;

class Profile extends React.Component {

    constructor(props) {
        super(props)
        this.state = {}
    }

    componentDidMount() {
        this.runJs();
    }

    componentWillReceiveProps(nextProps) {
        this.setState({isForMe: (Auth.isAuthenticated() && (nextProps.user.id == Auth.getUser().id))});
    }

    componentDidUpdate() {
        this.runJs();

    }

    runJs() {
    }

    render() {
        const {user} = this.props;
        let endDate = new Date((user.expire_at && user.expire_at.gregorian|| Date.now()));
        // let endDate = new Date("Tuesday 9 August 2017 13:54");
        endDate = endDate.getTime();

        console.log(endDate)

        return (
            <main className="profile">

                {/*Breadcrumb*/}
                <section className="breadcrumb-row">
                    <div className="container">
                        <ul className="breadcrumb">
                            <li><a href="/">خانه</a></li>
                            <li>پروفایل</li>
                        </ul>
                    </div>
                </section>
                {/*Breadcrumb*/}

                {/*ProfileStats*/}
                <section className="profile-stat-row container">
                    <div className="row">
                        <div className="each-stat col-lg-6 col-md-6 col-xs-12">
                            <div className="each-inner">
                                <i className="icon material-icons">&#xE866;</i>
                                <span>نشان شده ها</span>
                                <span className="count"><PersianNumber>{(user.bookmark_count || 0)}</PersianNumber> عدد </span>
                            </div>
                        </div>
                        <div className="each-stat col-lg-6 col-md-6 col-xs-12">
                            <div className="each-inner">
                                <i className="icon material-icons">&#xE8CC;</i>
                                <span>ویدیو های خریداری شده</span>
                                <span
                                    className="count"> <PersianNumber>{(user.buy_count || 0)}</PersianNumber> عدد </span>
                            </div>
                        </div>
                    </div>
                </section>
                {/*ProfileStats*/}

                {/*Money Bag*/}
                <section className="profile-money-bag row">
                    <div className="container">
                        <div className="blk-h">
                            <h2>
                                کیف پول
                            </h2>
                        </div>
                        <div className="blk-c">
                            <div className="row">
                                {/*row*/}
                                <div className="col-lg-12 col-md-12 col-xs-12">
                                    <div className="each-inner">
                                        <div className="money-each-stats">
                                            <div className="money-img"><img src="/static/img/report3.png" alt=""/></div>
                                            <strong>اعتبار بلیط</strong>
                                            <PersianNumber>{(user.tickets || 0)}</PersianNumber>
                                        </div>
                                        <div className="money-each-text">
                                            <p>
                                                با خرید بلیط شما میتوانید ویدیوهایی که پول هستند یا تماشا کنید.
                                                <br/>
                                                برای شارژ اعتبار بلیط خود وارد فروشگاه شوید.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                {/*row*/}
                                {/*row*/}
                                <div className="col-lg-12 col-md-12 col-xs-12">
                                    <div className="each-inner">
                                        <div className="money-each-stats">
                                            <div className="money-img"><img src="/static/img/report4.png" alt=""/></div>
                                            <strong>زمان باقی مانده بسته</strong>
                                            <Countdown date={endDate}>
                                                <Completionist/>
                                            </Countdown>
                                        </div>
                                        <div className="money-each-text">
                                            <p>
                                                با خرید بلیط شما میتوانید ویدیوهایی که پول هستند یا تماشا کنید.
                                                <br/>
                                                برای شارژ اعتبار بلیط خود وارد فروشگاه شوید.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                {/*row*/}
                                {/*row*/}
                                <div className="col-lg-12 col-md-12 col-xs-12 text-left">
                                    <Link prefetch route="myProfileShop">
                                        <a className="btn_nice_grad margin-left-0 margin-top-0">ورود به فروشگاه</a>
                                    </Link>
                                </div>
                                {/*row*/}
                            </div>
                        </div>
                    </div>
                </section>
                {/*Money Bag*/}

                {/*Buy Stats*/}
                <section className="profile-money-bag row">
                    <div className="container">
                        <div className="blk-h">
                            <h2>
                                گزارشات خرید
                            </h2>
                        </div>
                        <div className="buy-stat-row blk-c">
                            {/*ProfileStats*/}
                            <div className="row">
                                <div className="each-offer col-lg-3 col-md-3 col-xs-6">
                                    <div className="inner">
                                        <div className="thumbnail"
                                             style={{"backgroundImage": "url(" + "/static/img/cardticket.png" + ")"}}></div>
                                        <div className="details">
                                            <h2>بسته هدیه ویژه</h2>
                                            <div className="row">
                                                <div className="ticket-num">تعداد بلیط : <PersianNumber>
                                                    55</PersianNumber></div>
                                                {/*<div className="ticket-price"><PersianNumber> 5000 تومان </PersianNumber></div>*/}
                                                <div className="clear"></div>
                                            </div>
                                            <div className="ticket-desc">
                                                با خرید این بسته می توانید ویدئو را به صورت دائمی خریداری کنید.
                                            </div>
                                            <button className="btn_nice_grad block margin-bottom-5">خرید</button>
                                        </div>
                                    </div>
                                </div>
                                <div className="each-offer col-lg-3 col-md-3 col-xs-6">
                                    <div className="inner">
                                        <div className="thumbnail"
                                             style={{"backgroundImage": "url(" + "/static/img/cardticket.png" + ")"}}></div>
                                        <div className="details">
                                            <h2>بسته بلیط طلایی</h2>
                                            <div className="row">
                                                <div className="ticket-num">تعداد بلیط : <PersianNumber>
                                                    55</PersianNumber></div>
                                                <div className="ticket-price"><PersianNumber> 5000
                                                    تومان </PersianNumber></div>
                                                <div className="clear"></div>
                                            </div>
                                            <div className="ticket-desc">
                                                با خرید این بسته می توانید ویدئو را به صورت دائمی خریداری کنید.
                                            </div>
                                            <button className="btn_nice_grad block margin-bottom-5">خرید</button>
                                        </div>
                                    </div>
                                </div>
                                <div className="each-offer col-lg-3 col-md-3 col-xs-6">
                                    <div className="inner">
                                        <div className="thumbnail"
                                             style={{"backgroundImage": "url(" + "/static/img/cardticket.png" + ")"}}></div>
                                        <div className="details">
                                            <h2>بسته بلیط طلایی</h2>
                                            <div className="row">
                                                <div className="ticket-num">تعداد بلیط : <PersianNumber>
                                                    55</PersianNumber></div>
                                                <div className="ticket-price"><PersianNumber> 5000
                                                    تومان </PersianNumber></div>
                                                <div className="clear"></div>
                                            </div>
                                            <div className="ticket-desc">
                                                با خرید این بسته می توانید ویدئو را به صورت دائمی خریداری کنید.
                                            </div>
                                            <button className="btn_nice_grad block margin-bottom-5">خرید</button>
                                        </div>
                                    </div>
                                </div>
                                <div className="each-offer col-lg-3 col-md-3 col-xs-6">
                                    <div className="inner">
                                        <div className="thumbnail"
                                             style={{"backgroundImage": "url(" + "/static/img/cardticket.png" + ")"}}></div>
                                        <div className="details">
                                            <h2>بسته بلیط طلایی</h2>
                                            <div className="row">
                                                <div className="ticket-num">تعداد بلیط : <PersianNumber>
                                                    55</PersianNumber></div>
                                                <div className="ticket-price"><PersianNumber> 5000
                                                    تومان </PersianNumber></div>
                                                <div className="clear"></div>
                                            </div>
                                            <div className="ticket-desc">
                                                با خرید این بسته می توانید ویدئو را به صورت دائمی خریداری کنید.
                                            </div>
                                            <button className="btn_nice_grad block margin-bottom-5">خرید</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {/*ProfileStats*/}
                        </div>
                    </div>
                </section>
                {/*Buy Stats*/}

            </main>
        )
    }
}

export default Profile
