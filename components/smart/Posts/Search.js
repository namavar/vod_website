import React from 'react'
import Pager from '../../widgets/pagination/pager'
import {Link} from '../../../routes'
import api from '../../../utils/api'
import utils from '../../../utils/utils';
import Video from "../../widgets/video/video";



class Search extends React.Component {
    
    


  constructor(props) {
    super(props);
    this.PERPAGE = 12;
    this.state = {
       videoResults: props.videoResults || {},
       categoryList:props.categoryList || {} ,
       listOrderType: '',
       keyword : props.keyword,
       video_loading: false,
       params : {
         keyword:props.keyword,
         view_count:-1,
         page:1,
         per_page:this.PERPAGE,
         category:props.category || ''
       }
    };
    this.changeSearchOrder = this.changeSearchOrder.bind(this);
    this.detCheckVal = this.detCheckVal.bind(this);

  }

  componentWillReceiveProps(nextProps) {
      this.setState({
          videoResults: nextProps.videoResults || {},
          categoryList:nextProps.categoryList || {} ,
          keyword: nextProps.keyword,
          video_loading: false,
          params: {
              page:1,
              per_page:this.PERPAGE,
          }
      })
      $('.video-list.filters').find('li').each(function () {
          $(this).find('input[type="checkbox"]').prop('checked',false);
      })
  }

  componentDidMount() {

  }

  companiesPaginSearch(page) {
    this.setState({companiesLoading: true})
    api.get("companies", {
      'per-page': this.state.companiesPagination.perPage,
      page: page,
      content: (this.props.keyword && this.props.keyword.length >= 3) ? decodeURI(this.props.keyword) : ''
    }).then((res) => {
      this.setState({
        resultCompanies: res,
        companiesPagination: res._meta,
        companiesLoading: false
      });
      utils.closeSearch();
    });

  }

  getSearchResult() {
      this.setState({video_loading:true});
      api.get("videos", this.state.params ).then((res) => {
          this.setState({videoResults:res,video_loading:false});
      });
  }

  loadMore() {
      this.state.params.page++;
      this.setState({params:this.state.params});
      api.get("videos", this.state.params ).then((res) => {
          let newlist = this.state.videoResults.items.concat(res.items);
          this.state.videoResults.items = newlist;
          this.state.videoResults._links = res._links;
          this.state.videoResults._meta = res._meta;
          this.setState({videoResults:this.state.videoResults});
      });
  }

  detCheckVal(e) {
      var p = $(e.target).parents('.video-list');
      var cat_ids = p.find('input:checkbox:checked').map(function () {
         return $(this).val();
      });
      delete cat_ids.prevObject;
      let cats = [];
      for ( let i=0;i<=cat_ids.length;i++) {
          cats.push(cat_ids[i]);
      }
      cat_ids = cats.join();
      cat_ids = cat_ids.substring(0, cat_ids.length - 1);
      this.state.params.category = cat_ids;
      this.setState({params:this.state.params});
      console.log(this.state.params);
      this.getSearchResult();
  }

  searchByType() {
      let search_type = $('input[name="radiolistfilter"]:checked').val();

      switch (search_type) {
          case 'type_free' :
                  delete this.state.params.not_free;
                  this.state.params.is_free = 1;
                  // this.setState({params:this.state.params});
                  this.getSearchResult();
              break;
          case 'type_price':
                  delete this.state.params.is_free;
                  this.state.params.not_free = 1;
                  // this.setState({params:this.state.params});
                  this.getSearchResult();
              break;
          case 'type_all':
                  delete this.state.params.is_free;
                  delete this.state.params.not_free;
                  // this.setState({params:this.state.params});
                  this.getSearchResult();
              break;
          default:
                  delete this.state.params.is_free;
                  delete this.state.params.not_free;
                  // this.setState({params:this.state.params});
                  this.getSearchResult();
              break;

      }

  }

  changeSearchOrder(e) {

    switch (e.target.value) {
        case 'newest':
            this.state.params = {
                content: this.state.keyword || '',
                per_page: this.PERPAGE,
                page: 1,
                category: this.state.params.category || ''
            }
            this.setState({params:this.state.params});
            this.getSearchResult();
          break;
        case 'best':
            this.state.params = {
              content:this.state.keyword || '',
              rate:1,
              view_count:1,
              per_page:this.PERPAGE,
              page:1,
              category: this.state.params.category || ''
            }
            this.setState({params:this.state.params});
            this.getSearchResult();
          break;
        case 'view':
            this.state.params = {
                content:this.state.keyword || '',
                view_count:1,
                per_page:this.PERPAGE,
                page:1,
                category: this.state.params.category || ''
            }
            this.setState({params:this.state.params});
            this.getSearchResult();
          break;
        default:
            this.state.params = {
                content:this.state.keyword || '',
                per_page:this.PERPAGE,
                page:1,
                category: this.state.params.category || ''
            }
            this.setState({params:this.state.params});
            this.getSearchResult();
          break;
    }
  }

  render() {
    const {videoResults} = this.state;
    return (
      <main className="searchresult">
        <section className="breadcrumb-row">
          <div className="container">
            <ul className="breadcrumb">
              <li><a href="/">خانه</a></li>
              <li>ویدئوی منظومه احساس</li>
            </ul>
          </div>
        </section>
        <section className="main-content">
          <div className="container">
            <div className="row">
              <div className="video-page-l">
                <div className="video-page-l-in">
                  <div className={"blk new_video margin-bottom-0 " + ( (this.state.video_loading) ? 'loader' : '')}>
                    <div className="blk-h">
                      <h2>

                          نتایج جستجو
                          {this.state.keyword ? ' ' + this.state.keyword : ' همه'}

                      </h2>
                    </div>
                    <div className="blk-c">
                      <div className="row margin-bottom-15">
                        <div className="sort-text">مرتب سازی بر اساس :</div>
                        <div className="sort-select">
                          <div className="form-select">
                            <select onChange={this.changeSearchOrder}>
                              <option value="newest">جدیدترین ها</option>
                              <option value="best">برترین ها</option>
                              <option value="view">پربازدیدترین ها</option>
                            </select>
                          </div>
                        </div>
                        <div className="clear"></div>
                      </div>
                      <div className="row">
                          {
                              videoResults.items && videoResults.items.length>0 ?
                                  (
                                      videoResults.items.map((videoItem, index)=>{
                                          return (
                                              <div key={index} className="col-md-3  col-sm-4  col-xs-6 col-xxs-this.PERPAGE">
                                                  <Video type="grid" video={videoItem}/>
                                              </div>
                                          )
                                      })
                                  )
                                  :
                                  (
                                      <div className="resnotfound">
                                          <img src={"/static/img/logo.png"} alt=""/>
                                          <span>چیزی یافت نشد!</span>
                                      </div>
                                  )
                          }
                      </div>
                        {this.state.videoResults._links.next && <div className="row text-center">
                        <div className="col-md-this.PERPAGE">
                          <a className="load-more" onClick={()=> {
                              this.loadMore();
                          }}>
                            <div>مشاهده ویدئوهای بیشتر</div>
                            <i className="material-icons">&#xE313;</i>
                          </a>
                        </div>
                      </div>}
                    </div>
                  </div>
                </div>
              </div>
              <div className="video-page-r">

                  <div className="blk2">
                  <div className="blk2-h">
                    <h2>
                      نوع
                    </h2>
                  </div>
                  <div className="blk2-c">
                    <ul className="video-list filters">
                      <li>
                        <input onClick={()=> {this.searchByType()}} id="checkbox-all" className="checkbox-custom" value="type_all" name="radiolistfilter" type="radio" />
                        <label for="checkbox-all" className="checkbox-custom-label">همه</label>
                      </li>
                      <li>
                        <input onClick={()=> {this.searchByType()}} id="checkbox-free" className="checkbox-custom" value="type_free" name="radiolistfilter" type="radio" />
                        <label for="checkbox-free" className="checkbox-custom-label">رایگان</label>
                      </li>
                      <li>
                        <input onClick={()=> {this.searchByType()}} id="checkbox-price" className="checkbox-custom" value="type_price" name="radiolistfilter" type="radio" />
                        <label for="checkbox-price"className="checkbox-custom-label">پولی</label>
                      </li>
                    </ul>
                  </div>
                </div>
                  <div className="blk2">
                  <div className="blk2-h">
                    <h2>
                      دسته بندی ها
                    </h2>
                  </div>
                  <div className="blk2-c">

                    <ul className="video-list filters">
                      {/*radio button*/}
                      {/*<li>*/}
                        {/*<input id="radio-1" className="radio-custom" name="radio-group" type="radio" checked />*/}
                          {/*<label for="radio-1" className="radio-custom-label">First Choice</label>*/}
                      {/*</li>*/}

                        {this.state.categoryList && this.state.categoryList.map((item , index)=> {

                            return  (
                                <li key={index} onClick={this.detCheckVal}>
                                    <input id={"checkbox-" + index} className="checkbox-custom" name={"checkbox-" + index} value={item.name_latin} type="checkbox"  />
                                    <label for={"checkbox-" + index} className="checkbox-custom-label">{item.name}</label>
                                </li>
                            )

                        })}

                    </ul>
                  </div>

                </div>
              </div>
              <div className="clear"></div>
            </div>
          </div>
        </section>

      </main>
    )
  }
}

export default Search
