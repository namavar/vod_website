import React from 'react'
import {Link, Router} from '../../../routes'
import Video from "../../widgets/video/video";
import VideoPlayer from "../../widgets/video/VideoPlayer";
import PersianNumber from "../../widgets/input/PersianNumber";
import api from "../../../utils/api";
import Auth from "../../../services/AuthService";
import Config from "../../../config/config";


class VideoSingle extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            video: props.video || {},
            relatedVideos: props.relatedVideos,
            is_related_load_more: (props.relatedVideos._links && props.relatedVideos._links.next),
            is_like: props.video.liked || false,
            is_bookmark: props.video.bookmarked || false,
            location: ''
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            video: nextProps.video || {},
            relatedVideos: nextProps.relatedVideos,
            is_like: nextProps.video.liked || false,
            is_bookmark: nextProps.video.bookmarked || false,
            location: window.location.href
        })
    }

    componentWillMount() {
        setTimeout(() => {
            const isBrowser = typeof window !== 'undefined';
            if (isBrowser) {
                this.setState({location: window.location.href})
            }
        }, 500)
    }

    likeVideo() {
        if (Auth.isAuthenticated()) {
            if (this.state.is_like)
                api.delete('videos/' + this.state.video.id + '/like', {id: this.state.video.id}).then((success) => {
                    this.state.video.like_count = Number(this.state.video.like_count || 1) - 1;
                    this.setState({is_like: false});
                })
            else
                api.post('videos/' + this.state.video.id + '/like', {id: this.state.video.id}).then((success) => {
                    this.state.video.like_count = Number(this.state.video.like_count) + 1;
                    this.setState({is_like: true});
                })
        }
        else
            $('#login_modal,.c_modal_overlay').addClass('active');
    }

    bookmarkVideo() {
        if (Auth.isAuthenticated()) {
            if (this.state.is_bookmark)
                api.delete('videos/' + this.state.video.id + '/bookmark', {id: this.state.video.id}).then((success) => {
                    this.setState({is_bookmark: false});
                })
            else
                api.post('videos/' + this.state.video.id + '/bookmark', {id: this.state.video.id}).then((success) => {
                    this.setState({is_bookmark: true});
                })
        }
        else
            $('#login_modal,.c_modal_overlay').addClass('active');
    }

    loadMoreVideo() {
        if (this.state.relatedVideos && this.state.relatedVideos._links && this.state.relatedVideos._links.next)
            api.get(this.state.relatedVideos._links.next.href.replace(Config.API, "")).then((res) => {
                this.state.relatedVideos.items = this.state.relatedVideos.items.concat(res.items)
                this.state.relatedVideos._links = res._links;
                this.setState({
                    relatedVideos: this.state.relatedVideos,
                    is_related_load_more: (this.state.relatedVideos._links && this.state.relatedVideos._links.next)
                });
            })
    }

    componentDidMount() {
        this.runJs();
    }

    runJs() {
        setTimeout(() => {
            $('.share-btn').click(function (e) {
                $('#popover-content').toggleClass('active');
                e.stopPropagation();
            })
            $('#popover-content *').click(function (e) {
                e.stopPropagation();
            });
            $(document).click(function () {
                $('#popover-content').removeClass('active');
            })
        }, 300);

    }

    render() {
        const {relatedVideos, is_like, is_bookmark, is_related_load_more} = this.state;
        const {video} = this.props;
        return (
            <main className="video-page">
                <section className="breadcrumb-row">
                    <div className="container">
                        <ul className="breadcrumb">
                            <li><a href="/">خانه</a></li>
                            <li>{video.name}</li>
                        </ul>
                    </div>
                </section>
                <section className="main-content">
                    <div className="container">
                        <div className="row">
                            <div className="video-page-l">

                                <div className="videoHolder">
                                    { !video.file_name && <div className="darklayer"></div> }
                                    { !video.file_name &&  <div className="previmage" style={{"background-image": "url(" + (video.image.large || video.image.small || video.image) + ")"}}></div>}
                                    { video.file_name &&  <div>
                                        <VideoPlayer
                                            file={video.file_name}
                                            image={video.image}
                                            type={video.type}
                                            width={"100%"}
                                            aspectratio={"16:9"}
                                            abouttext="VOD Web Site"
                                            aboutlink="/"
                                            autostart
                                        />
                                    </div> }
                                </div>

                                <div className="row video-details">
                                    <div className="col-md-12">
                                        {!video.file_name &&
                                        <button className="btn_nice_grad pull-left margin-left-0">خرید ویدئو</button> }
                                        <h1 className="pull-right">{video.name}</h1>
                                        <div className="clear"></div>
                                    </div>
                                    <div className="col-md-12">
                                        <div className="blk2">
                                            <div className="blk2-h">
                                                <div className="row">
                                                    <div className="col-sm-2">
                                                        <div className="view">
                                                            <i className="material-icons">&#xE8F4;</i>
                                                            <span>
                                                              <span> بازدید </span>
                                                              <PersianNumber
                                                                  className="num">{video.view_count || 1}</PersianNumber>
                                                            </span>

                                                        </div>
                                                    </div>
                                                    <div className="col-sm-10">
                                                        <div className="video-action">
                                                            <ul>
                                                                <li>
                                                                    <a id="like_btn" className={"cpointer " + (is_like ? "liked" : "")}
                                                                       onClick={() => {
                                                                           this.likeVideo();
                                                                       }}>
                                                                        <i className="material-icons">&#xE8DC;</i>
                                                                        <PersianNumber
                                                                            className="num">{(video.like_count || 0)}</PersianNumber>
                                                                        <div className="clear"></div>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" id="download_btn">
                                                                        <i className="material-icons">&#xE2C4;</i>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    {/*<a href="#">*/}
                                                                    {/*<i className="material-icons">&#xE80D;</i>*/}
                                                                    {/*</a>*/}
                                                                    <a className="share-btn" title="">
                                                                        <i className="material-icons">&#xE80D;</i>
                                                                    </a>
                                                                    <div id="popover-content">
                                                                        <a data-original-title="Facebook" target="blank"
                                                                           href={"http://www.facebook.com/share.php?u=" + this.state.location}
                                                                           data-toggle="tooltip" data-placement="top"
                                                                           title="">
                                                                            <img src="/static/img/facebook.svg"
                                                                                 alt="facebook" width="30"/>
                                                                        </a>
                                                                        <a data-original-title="Google plus"
                                                                           target="blank"
                                                                           href={"https://plus.google.com/share?url=" + this.state.location}
                                                                           data-toggle="tooltip" data-placement="top"
                                                                           title="">
                                                                            <img src="/static/img/google-plus.svg"
                                                                                 alt="google-plus" width="30"/>
                                                                        </a>
                                                                        <a data-original-title="Linked in"
                                                                           target="blank"
                                                                           href={"https://www.linkedin.com/shareArticle?mini=true&url=" + this.state.location}
                                                                           data-toggle="tooltip" data-placement="top"
                                                                           title="">
                                                                            <img src="/static/img/linkedin.svg"
                                                                                 alt="linkedin" width="30"/>
                                                                        </a>
                                                                        <a data-original-title="telegram" target="blank"
                                                                           href={"https://t.me/share/url?url=" + this.state.location}
                                                                           data-toggle="tooltip" data-placement="top"
                                                                           title="">
                                                                            <img src="/static/img/telegram.svg"
                                                                                 alt="telegram" width="30"/>
                                                                        </a>
                                                                        <div id="popover-arrow"></div>

                                                                    </div>

                                                                </li>
                                                                <li>
                                                                    <a id="bookmark_btn" className={"cpointer " + (is_bookmark ? "liked" : "")}
                                                                       onClick={() => {
                                                                           this.bookmarkVideo();
                                                                       }}>
                                                                        <i className="material-icons">&#xE866;</i>
                                                                    </a>
                                                                </li>

                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="blk2-c">
                                                <p>{video.description || ""}</p>
                                            </div>
                                            {video.tags && video.tags.length > 0 && <div className="tag-row">
                                                <h2>تگ های مرتبط</h2>

                                                {
                                                    video.tags.map((item, i) => {
                                                        return <Link key={i} prefetch route='search' params={{keyword: item}} title={item}><a className="tag">{item}</a></Link>
                                                    })
                                                }

                                            </div>}
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div className="video-page-r">
                                <div className="blk2">
                                    <div className="blk2-h">
                                        <h2>
                                            ویدئوهای مرتبط
                                        </h2>
                                    </div>
                                    <div className="blk2-c">
                                        <ul className="video-list">
                                            {
                                                relatedVideos && relatedVideos.items && relatedVideos.items.map((item, index) => {
                                                    return (
                                                        <li key={index}><Video video={item}/></li>
                                                    )
                                                })
                                            }

                                        </ul>
                                    </div>
                                    { is_related_load_more && <div className="row text-center margin-top-30">
                                        <div className="col-md-12">
                                            <a className="load-more" onClick={() => this.loadMoreVideo()}>
                                                <div>مشاهده ویدئوهای بیشتر</div>
                                                <i className="material-icons">&#xE313;</i>
                                            </a>
                                        </div>
                                    </div>}
                                </div>
                            </div>
                            <div className="clear"></div>
                        </div>
                    </div>
                </section>
            </main>
        )
    }
}

export default VideoSingle
