import React from 'react'
import {Link, Router} from '../../../routes'
import Video from "../../widgets/video/video";

import api from '../../../utils/api'

class Category extends React.Component {

    constructor(props) {
        super(props)
        this.PERPAGE = 10;
        this.state = {
            videoResults:props.videos,
            params: {
                category:props.categoryId,
                page:1,
                per_page:this.PERPAGE,
            }
        }
    }

    componentDidMount() {
        this.runJs();
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            videoResults:nextProps.videos,
            params: {
                category:nextProps.categoryId,
                page:1,
                per_page:this.PERPAGE,
            }
        })
    }

    componentDidUpdate() {
        this.runJs();
    }

    runJs() {
        setTimeout(() => {

            $(document).ready(() => {


            });

        }, 200)
    }

    loadMore() {
        this.state.params.page++;
        this.setState({params:this.state.params});
        api.get("videos", this.state.params ).then((res) => {
            let newlist = this.state.videoResults.items.concat(res.items);
            this.state.videoResults.items = newlist;
            this.state.videoResults._links = res._links;
            this.state.videoResults._meta = res._meta;
            this.setState({videoResults:this.state.videoResults});
        });
    }


    render() {
        const {videos} = this.props;
        return (
            <main className="videoList">
                <section className="breadcrumb-row">
                    <div className="container">
                        <ul className="breadcrumb">
                            <li><a href="/">خانه</a></li>
                            <li><a href="#">دسته بندی</a></li>
                            <li>{this.state.params.category}</li>
                        </ul>
                    </div>
                </section>
                <section className="main-content padding-top-15 padding-bottom-0">
                    <div className="container">
                        {

                            (videos.items && videos.items.length > 0) &&
                            <div className="blk new_video margin-bottom-0">
                                <div className="blk-h">
                                    <h2>
                                        <span>دسته بندی </span>
                                        {this.state.params.category}
                                    </h2>
                                </div>
                                <div className="blk-c">
                                    <div className="row no-margin">

                                        {
                                            videos.items.map((item, index) => {
                                                return (
                                                   <div key={index} className="col-lg-5th col-md-5th col-sm-5th col-xs-5th">
                                                        <Video type="grid" video={item} key={index}/>
                                                   </div>
                                                 )
                                            })
                                        }


                                    </div>
                                    {this.state.videoResults._links.next && <div className="row text-center">
                                        <div className="col-md-12">
                                            <a className="load-more" onClick={()=> {
                                                this.loadMore();
                                            }}>
                                                <div>مشاهده ویدئوهای بیشتر</div>
                                                <i className="material-icons">&#xE313;</i>
                                            </a>
                                        </div>
                                    </div>}
                                </div>
                            </div>
                        }
                    </div>
                </section>
            </main>
        )
    }
}

export default Category
