import React from 'react'
import api from '../../../utils/api'
import Auth from '../../../services/AuthService'
import PersianNumber from "../../widgets/input/PersianNumber";
import {Link, Router} from '../../../routes'
import config from "../../../config/config";
import client from "../../../services/Events";
import LoadingButton from "../../widgets/input/LoadingButton";


const LOGIN_INIT = "showlogin";

class ProfileShop extends React.Component {

    constructor(props) {
        super(props)
        this.state = {}
    }

    componentDidMount() {
        this.runJs();
    }

    componentWillReceiveProps(nextProps) {
        this.setState({isForMe: (Auth.isAuthenticated() && (nextProps.user.id == Auth.getUser().id))});
    }

    componentDidUpdate() {
        this.runJs();
    }

    buyTicket(id, index) {
        let btn = '#buyBtn' + index;
        $(btn).addClass('loader')
        api.post('tickets/' + id + '/buy/ipg').then((res) => {
            if (res.url)
                window.location = res.url;
            $(btn).removeClass('loader')
        })
    }

    runJs() {
        setTimeout(() => {

            $(document).ready(() => {
                $('.tabs li').click(function () {
                    var tab_target = $(this).attr('data-target');
                    $('.tabs li').removeClass('active');
                    $(this).addClass('active');
                    $('.tab_contents .tab_content').removeClass('active');
                    $('#' + tab_target).addClass('active');
                });
            });

        }, 200)
    }

    render() {
        const {products} = this.props;
        return (
            <main className="profile shop">

                {/*Breadcrumb*/}
                <section className="breadcrumb-row">
                    <div className="container">
                        <ul className="breadcrumb">
                            <li><a href="/">خانه</a></li>
                            <li>پروفایل</li>
                        </ul>
                    </div>
                </section>
                {/*Breadcrumb*/}

                {/*ProfileStats*/}
                <section className="profile-stat-row container">
                    <div className="row">
                        <div className="each-stat col-lg-6 col-md-6 col-xs-12">
                            <div className="each-inner">
                                <div className="each-aside text-center">
                                    <div className="money-each-stats">
                                        <div className="iconholder">
                                            <img src="/static/img/walletticket.png" alt=""/>
                                        </div>
                                        <strong>اعتبار بلیط</strong>
                                        <PersianNumber>۲۵</PersianNumber>
                                    </div>
                                </div>
                                <div className="each-aside text-center">
                                    <div className="money-each-stats">
                                        <div className="iconholder">
                                            <img src="/static/img/wallettime.png" alt=""/>
                                        </div>
                                        <strong>زمان باقی مانده بسته</strong>
                                        <PersianNumber>۰۰:۰۰</PersianNumber>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="each-stat gift-code col-lg-6 col-md-6 col-xs-12">
                            <div className="each-inner">
                                <img className="icon-gift" src="/static/img/walletgift.png" alt=""/>
                                <input type="text" placeholder="اگر کد هدیه دارید اینجا وارد کنید"/>
                                <button className="btn_nice_grad margin-left-0 margin-top-0" type="button">دریافت کد
                                    هدیه
                                </button>
                            </div>
                        </div>
                    </div>
                </section>
                {/*ProfileStats*/}

                {/*Money Bag*/}
                <section className="profile-money-bag row">
                    <div className="container">
                        <div className="blk-c">
                            {/*tabs*/}

                            <ul className="tabs">
                                <li className="active" data-target="ticketPack">
                                    <a className="cpointer">بسته های ویژه بلیطی</a>
                                </li>
                                <li data-target="timePack">
                                    <a className="cpointer">بسته های ویژه زمانی</a>
                                </li>
                            </ul>

                            {/*tabs*/}


                            {/*tab_content*/}
                            <div className="tab_contents">
                                <div className="tab_content active" id="ticketPack">
                                    {/*row*/}

                                    {
                                        products.map((item, i) => {
                                            return (
                                                !item.is_date &&
                                                <div key={i} className="each-offer col-lg-3 col-md-3 col-xs-6">
                                                    <div className="inner">
                                                        <div className="thumbnail"
                                                             style={{"backgroundImage": "url(" + (item.icon || "/static/img/cardticket.png") + ")"}}></div>
                                                        <div className="details">
                                                            <h2>{item.name}</h2>
                                                            <div className="row">
                                                                <div className="ticket-num">تعداد بلیط :
                                                                    <PersianNumber>{item.ticket}</PersianNumber></div>
                                                                <div className="ticket-price">
                                                                    <PersianNumber> {item.price} تومان </PersianNumber>
                                                                </div>
                                                                <div className="clear"></div>
                                                            </div>
                                                            <div className="ticket-desc">
                                                                {item.description}
                                                            </div>
                                                            <button id={'buyBtn' + i} onClick={() => {
                                                                this.buyTicket(item.id, i)
                                                            }}
                                                                    className="btn_nice_grad block margin-bottom-5">خرید
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            )
                                        })
                                    }
                                    {/*row*/}

                                </div>
                                <div className="tab_content" id="timePack">

                                    {
                                        products.map((item, i) => {
                                            return (
                                                item.is_date &&
                                                <div key={i} className="each-offer col-lg-3 col-md-3 col-xs-6">
                                                    <div className="inner">
                                                        <div className="thumbnail"
                                                             style={{"backgroundImage": "url(" + (item.icon || "/static/img/cardticket.png") + ")"}}></div>
                                                        <div className="details">
                                                            <h2>{item.name}</h2>
                                                            <div className="row">
                                                                <div className="ticket-num">تعداد روز :
                                                                    <PersianNumber>{item.day}</PersianNumber></div>
                                                                <div className="ticket-price">
                                                                    <PersianNumber> {item.price} تومان </PersianNumber>
                                                                </div>
                                                                <div className="clear"></div>
                                                            </div>
                                                            <div className="ticket-desc">
                                                                {item.description}
                                                            </div>
                                                            <button id={'buyBtn' + i} onClick={() => {
                                                                this.buyTicket(item.id, i)
                                                            }}
                                                                    className="btn_nice_grad block margin-bottom-5">
                                                                خرید
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            )
                                        })
                                    }
                                </div>
                            </div>
                            {/*tab_content*/}
                        </div>
                    </div>
                </section>
                {/*Money Bag*/}


            </main>
        )
    }
}

export default ProfileShop
