/**
 * Created by miladh on 5/22/17.
 */
import React from 'react';
import {Link} from '../../../routes';
class Footer extends React.Component {
  render() {
    return (
      <footer className="main-footer">
        <div className="container">
          <div className="row">
            {/*<div className="col-md-6">*/}
              {/*<ul>*/}
                {/*<li>*/}
                  {/*<Link route="About">*/}
                    {/*<a className="cpointer">*/}
                      {/*درباره ما*/}
                    {/*</a>*/}
                  {/*</Link>*/}
                {/*</li>*/}
                {/*<li>*/}
                  {/*<Link route="contact">*/}
                    {/*<a className="cpointer">*/}
                      {/*تماس با ما*/}
                    {/*</a>*/}
                  {/*</Link>*/}
                {/*</li>*/}
                {/*<li>*/}
                  {/*<Link route="Rules">*/}
                    {/*<a className="cpointer">*/}
                      {/*قوانین و مقررات*/}
                    {/*</a>*/}
                  {/*</Link>*/}
                {/*</li>*/}
                {/*<li>*/}
                  {/*<Link route="Privacy">*/}
                    {/*<a className="cpointer">*/}
                      {/*حریم خصوصی*/}
                    {/*</a>*/}
                  {/*</Link>*/}
                {/*</li>*/}
              {/*</ul>*/}
            {/*</div>*/}
            <div className="col-md-12">
              <div className="text-left line-height-24 ltr">&#169; 2013-2017 VOD website.All rights reserved.</div>
            </div>
          </div>
        </div>
      </footer>
    );
  }
}

export default Footer
