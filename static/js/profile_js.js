/**
 * Created by mahdir on 5/22/17.
 */
$(document).ready(function(){

  //Modals
  $('.add_comment').click(function () {
    $('.c_modal#add_comment_modal').addClass('active');
  });
  $('.c_modal_overlay').click(function () {
    $('.c_modal').removeClass('active');
  });
  $('.close_modal').click(function () {
    $('.c_modal').removeClass('active');
  })

  document.lastDisTab = 0;
  document.tabFixedChecker = false;
  $(window).scroll(function () {
    var dis_tab_top = $('#tabsholder').offset().top;
    var scrollTop = $(window).scrollTop();
    //-20 for padding_section_parent
    if ( scrollTop >= dis_tab_top - 20 && !document.tabFixedChecker ) {
      document.lastDisTab = dis_tab_top + 20;
      $('#tabsholder').addClass('fixed');
      $('#languages').css('margin-top','102px');
      document.tabFixedChecker = true;
    } else if ( scrollTop < document.lastDisTab - 20 && document.tabFixedChecker ) {
      $('#tabsholder').removeClass('fixed');
      $('#languages').css('margin-top','50px');
      document.tabFixedChecker = false;
    }
  });
});
