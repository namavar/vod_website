var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var minify = require('gulp-minify-css');
var inject = require('gulp-inject');
var gutil = require('gulp-util');

var headerScripts= [
    'static/js/jquery-3.2.1.min.js'
]

var scripts= [
    'static/js/jquery.growl.js',
    'static/js/preloader.js',
    'static/js/jw7.10/jwplayer.js',
    'static/js/jw7.10/jwp.js',
    'static/js/jw7.10/provider.hlsjs.js',
]

var styles= [
    'static/css/c_bootstrap.css',
    'static/css/jquery.growl.css',
    'static/css/nprogress.css',
    'static/css/style.css',
]

gulp.task('js', function () {
    gulp.src(scripts)
        .pipe(concat('main.js'))
        .pipe(uglify())
        .pipe(gulp.dest('static/js/'));
});
gulp.task('css', function () {
    gulp.src(styles)
        .pipe(concat('main.css'))
        .pipe(minify())
        .pipe(gulp.dest('static/css/'));
});

gulp.task('inject', function () {
    var target = gulp.src('./pages/_document.js');
    // It's not necessary to read the files (will speed up things), we're only after their paths:
    var sources = gulp.src((gutil.env.NODE_ENV !== 'production') ? ([].concat(scripts, styles)) : ([].concat(['./static/css/main.css', './static/js/main.js'])), {read: false});
    var headSources = gulp.src([].concat(headerScripts), {read: false});

    return target
        .pipe(inject(sources, {starttag:" {/* {{name}}:{{ext}} */}", endtag: ' {/* endinject */}', selfClosingTag:true}))
        .pipe(inject(headSources, {starttag:" {/* {{name}}:head:{{ext}} */}", endtag: ' {/* endinject */}', selfClosingTag:true}))
        .pipe(gulp.dest('./pages'));
});

gulp.task('inject-prod', function () {
    var target = gulp.src('./pages/_document.js');
    // It's not necessary to read the files (will speed up things), we're only after their paths:
    var sources = gulp.src(['./static/css/main.css', './static/js/main.js'], {read: false});
    var headSources = gulp.src(headerScripts, {read: false});

    return target
        .pipe(inject(sources, {starttag:" {/* {{name}}:{{ext}} */}", endtag: ' {/* endinject */}', selfClosingTag:true}))
        .pipe(inject(headSources, {starttag:" {/* {{name}}:head:{{ext}} */}", endtag: ' {/* endinject */}', selfClosingTag:true}))
        .pipe(gulp.dest('./pages'));
});




gulp.task('dev', ['inject'], function () {});
gulp.task('prod', ['js', 'css', 'inject-prod'], function () {});

