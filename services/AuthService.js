import utils from '../utils/utils';
import api from '../utils/api';
import config from './../config/config'
import cookie from 'react-cookie'
import NProgress from 'nprogress'
import Router from 'next/router'


class AuthService {


  login(mobile) {
    return api.post('auth/login', mobile)
  }

  resendCode(id, sec_code) {
    return api.post('auth/activate/'+id+'/resend', {security_code:sec_code})
  }

  signup(mobile, name, lastname) {
    return api.post('auth/register', {
      mobile, name, lastname
    })
  }

  activeCode(params) {
    return api.post('auth/activate/' + params.id, params)
  }

  async fetchUser() {
    let token = cookie.load('token')
    if (!token) {
      return
    }
    try {
      return api.get('users')
    } catch (e) {
      return Promise.reject(e)
    }
  }

  async loadUser({req, res}) {
    if (!req) {
      let localUser = JSON.parse(window.localStorage.getItem('user'))
      if (localUser && localUser.user) {
        return {
          user: localUser.user
        }
      }
    }
    if (req) {
      cookie.setRawCookie(req.headers.cookie)
    }
    try {
      const user = await this.fetchUser()
      return user
    } catch (e) {
      return Promise.reject(e)
    }
  }

   logout() {
    NProgress.start()
    try {
      cookie.remove('token', {
        path: '/',
      })
      utils.removeLocalStorage("user");
      NProgress.done();
      Router.pushRoute('index')
    } catch (e) {
      NProgress.done()
      throw e
    }
  }


  saveUserWithToken(user) {
    cookie.save('token', user.token, {path: '/'})
    utils.setLocalStorage("user", user.user);
  }

  getToken() {
    let token = cookie.load('token')
      return token;
  }

  getUser() {
    let user = utils.getLocalStorage("user")
    if (user)
      return user;
  }

  isAuthenticated() {
    return (utils.getLocalStorage("user") && cookie.load('token'));
  }

}

export default new AuthService()
